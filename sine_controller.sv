module sine_controller(
                      input                  clk,
                      input                  reset_n,  // Active-low reset
							 input logic 				key_ready,
							 input  logic[2:0]      tone,
							 input  logic[9:0] 		sample_num,
							 input logic init_done,
							 input logic data_over,
							 output logic [15:0] audio_packet
       );

enum logic [3:0] {IDLE, GET_KEY, LOAD_TONE, PLAY,DONE_PLAY,ADSR_UPDATE, CHECK_COUNT} state, next_state;
logic [15:0] 	RDATA,LDATA;
logic [9:0] counter, counter_in;
logic [15:0] a,b,c,d,e,f,g;
logic [2:0] tone_out;
logic Load;
assign LDATA = RDATA;
assign audio_packet = RDATA;

//Things for adsr
logic[2:0] adsr_state, adsr_state_in;
logic key_release, att_done, rel_done, dec_done, sus_done;
logic[15:0] adsr_sample, adsr_sample_in;
logic[31:0] adsr_fp, adsr_fp_in, adsr_fp_out;


reg_3 register(	.Clk(clk),
						.Reset(~reset_n),
						.Load(Load),
						.D(tone),
						.Data_Out(tone_out)
					);

					
// This instantiation is for week 1. For week 2, instantiate AES according to new interfaces.
A_3	a0(.in(counter),.dval(a));
B_3	b0(.in(counter),.dval(b));
C_3 	c0(.in(counter),.dval(c));
D_3 	d0(.in(counter),.dval(d));
E_3 	e0(.in(counter),.dval(e));
F_3 	f0(.in(counter),.dval(f));
G_3 	g0(.in(counter),.dval(g));

A_4	a0(.in(counter),.dval(a));
B_4	b0(.in(counter),.dval(b));
C_4 	c0(.in(counter),.dval(c));
D_4 	d0(.in(counter),.dval(d));
E_4 	e0(.in(counter),.dval(e));
F_4 	f0(.in(counter),.dval(f));
G_4 	g0(.in(counter),.dval(g));

A_5	a0(.in(counter),.dval(a));
B_5	b0(.in(counter),.dval(b));
C_5 	c0(.in(counter),.dval(c));
D_5 	d0(.in(counter),.dval(d));
E_5 	e0(.in(counter),.dval(e));
F_5 	f0(.in(counter),.dval(f));
G_5 	g0(.in(counter),.dval(g));


adsr_controller adsr_0 (.adsr_state(adsr_state),
								.adsr_fp(adsr_fp),
								.adsr_fp_out(adsr_fp_out),
								.sample(adsr_sample_in),
								.key_release(~key_ready),
								.sample_out(RDATA),
								.att_done(att_done),.dec_done(dec_done),.sus_done(sus_done),.rel_done(rel_done));

tone_mux tone0(.a(a),
					.b(b),
					.c(c),
					.d(d),
					.e(e),
					.f(f),
					.g(g),
					.select(tone_out),
					.sample(adsr_sample_in));
							  

always_ff @ (posedge clk) begin
    if (~reset_n) begin
		  counter = 10'b0;
        state <= IDLE;
		  adsr_state <= 3'b000;
		  adsr_fp <= 32'd0;
    end 
    else begin
		  counter <= counter_in;
        state <= next_state; 
		  adsr_fp <= adsr_fp_in;
		  adsr_state <= adsr_state_in;
    end
end

always_comb begin
    // Next state logic
    next_state = state;
    case (state)
        // Wait until IO is ready.
        IDLE: begin
            if (init_done)
                next_state = GET_KEY;
        end
			GET_KEY: begin	
				if(key_ready)
					next_state = LOAD_TONE;
			end
        // Wait for AES decryption until it is ready.
        // (The counter prevents getting stuck in this state.)
        LOAD_TONE: begin
             next_state = PLAY;
        end
		  PLAY: begin
					next_state = DONE_PLAY;
			end
		  DONE_PLAY:begin
					if(data_over)
						next_state = ADSR_UPDATE;
		  end
		  //wait for sample process to run
		  ADSR_UPDATE: begin
				if(rel_done) begin
					next_state = GET_KEY;
				end
				else begin
					next_state = PLAY;
				end
			end
        CHECK_COUNT: begin
				next_state = PLAY;
        end
    endcase
	 
    // Control signals
	 counter_in = counter;
	 Load = 1'b0;
	 adsr_state_in = adsr_state;
	 adsr_fp_in = adsr_fp;
    case (state)
        IDLE: begin
	
        end
		  GET_KEY: begin
			  counter_in = 10'b0;
			  adsr_fp_in = 32'd0;
			  adsr_state_in = 3'b000;
	
		  end 
		  LOAD_TONE: begin
				Load = 1'b1;

        end
		  PLAY: begin
				Load = 1'b0;
				counter_in = counter;
		
		  end
        DONE_PLAY:begin

		  end
		  ADSR_UPDATE: begin
				if(att_done) begin
					adsr_state_in = 3'b001;
				end
				else if(dec_done) begin 
					adsr_state_in = 3'b010;
				end
				else if(sus_done) begin
					adsr_state_in = 3'b011;
				end
				adsr_fp_in = adsr_fp_out;
				if(counter != sample_num) begin
					counter_in = counter + 1'b1;	
				end	
				else if(counter == sample_num) begin
					counter_in = 10'd0;
				end
			end
			default:;
    endcase
end
     
endmodule

