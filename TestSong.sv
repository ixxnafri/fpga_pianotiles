module drop_mux2(	input	Clk,
						Reset,
						frame_clk, 
						SW,
						output logic 	key_1,
											key_2,
											key_3,
											key_4,
											key_5,
											key_6,
											key_7,
											key_8,
											key_9,
											key_10,
											key_11,
											key_12,
											key_13,
											key_14,
											key_15,
											key_16);

    logic frame_clk_delayed, frame_clk_rising_edge;
	 
	 
    always_ff @ (posedge Clk) begin
        frame_clk_delayed <= frame_clk;
        frame_clk_rising_edge <= (frame_clk == 1'b1) && (frame_clk_delayed == 1'b0);
    end
	 
	 
	 
	 logic counter_30_sec;
	 logic [4:0] counter;
	 always_ff @ (posedge Clk)
    begin
		if (frame_clk_rising_edge)
		counter <= counter + 5'd1;
		
		else if(counter == 5'd20)
		begin
		counter <= 5'd0;
		counter_30_sec <= counter_30_sec + 1'b1;
		end
		else;
	end
		

	
	always_ff @ (posedge counter_30_sec) 
	begin
	if (Reset) 
	begin
        state <= WAIT;
    end 
    else if (SW == 7'd2)
	 begin
        state <= next_state; 
    end
	 else
		begin
			state <= START_1;
		end
	end
	
	enum logic [6:0] {WAIT, START_1, ONE_1, ONE_2, ONE_3, ONE_4, ONE_5, ONE_6, ONE_7,ONE_8, ONE_9,
							ONE_10, ONE_11,ONE_12,ONE_13,ONE_14,ONE_15,ONE_16, ONE_17, ONE_18,ONE_19,ONE_20,
							ONE_21,ONE_22,ONE_23,ONE_24,ONE_25,ONE_26,ONE_27,ONE_28,ONE_29,ONE_30} state, next_state;
							
	always_ff@(posedge Clk) 
	begin
   unique case (state)
	WAIT:;
		START_1: 
			next_state <= ONE_1;
		ONE_1:
			next_state <= ONE_2;
		ONE_2: 
			next_state <= ONE_3;
		ONE_3: 
			next_state <= ONE_4;
		ONE_4: 
			next_state <= ONE_5;
		ONE_5: 
			next_state <= ONE_6;
		ONE_6: 
			next_state <= ONE_7;
		ONE_7: 
			next_state <= ONE_8;
		ONE_8: 
			next_state <= ONE_9;
		ONE_9: 
			next_state <= ONE_10;
		ONE_10: 
			next_state <= ONE_11;
		ONE_11: 
			next_state <= ONE_12;
		ONE_12: 
			next_state <= ONE_13;
		ONE_13: 
			next_state <= ONE_14;
		ONE_14: 
			next_state <= ONE_15;
		ONE_15: 
			next_state <= ONE_16;
		ONE_16: 
			next_state <= ONE_17;
		ONE_17: 
			next_state <= ONE_18;
		ONE_18: 
			next_state <= ONE_19;
		ONE_19: 
			next_state <= ONE_20;
		ONE_20: 
			next_state <= ONE_21;
		ONE_21: 
			next_state <= ONE_22;
		ONE_22: 
			next_state <= ONE_23;
		ONE_23: 
			next_state <= ONE_24;
		ONE_24: 
			next_state <= ONE_25;
		ONE_25: 
			next_state <= ONE_26;
		ONE_26: 
			next_state <= ONE_27;
		ONE_27: 
			next_state <= ONE_28;
		ONE_28: 
			next_state <= ONE_29;
		ONE_29: 
			next_state <= ONE_30;
		ONE_30: 
			next_state <= WAIT;
	 endcase
end

always_comb 
begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	 
	 case(state)
	 
	WAIT:
		begin
		key_1 = 1'b0;
		key_2 = 1'b0;
		key_3 = 1'b0;
		key_4 = 1'b0;
		key_5 = 1'b0;
		key_6 = 1'b0;
		key_7 = 1'b0;
		key_8 = 1'b0;
		key_9 = 1'b0;
		key_10 = 1'b0;
		key_11 = 1'b0;
		key_12 = 1'b0;
		key_13 = 1'b0;
		key_14 = 1'b0;
		key_15 = 1'b0;
		key_16 = 1'b0;
		end
	START_1:
		begin
		key_1 = 1'b0;
		key_2 = 1'b0;
		key_3 = 1'b0;
		key_4 = 1'b0;
		key_5 = 1'b0;
		key_6 = 1'b0;
		key_7 = 1'b0;
		key_8 = 1'b0;
		key_9 = 1'b0;
		key_10 = 1'b0;
		key_11 = 1'b0;
		key_12 = 1'b0;
		key_13 = 1'b0;
		key_14 = 1'b0;
		key_15 = 1'b0;
		key_16 = 1'b0;
		end	
	ONE_1:
		begin
		key_1 = 1'b1;
		key_2 = 1'b0;
		key_3 = 1'b0;
		key_4 = 1'b0;
		key_5 = 1'b0;
		key_6 = 1'b0;
		key_7 = 1'b0;
		key_8 = 1'b0;
		key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_2:
		begin
		key_1 = 1'b0;
		key_2 = 1'b1;
		key_3 = 1'b0;
		key_4 = 1'b0;
		key_5 = 1'b0;
		key_6 = 1'b0;
		key_7 = 1'b0;
		key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end	
	
	ONE_3: 
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b1;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_4: 
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b1;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_5:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b1;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	ONE_6: 
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b1;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		ONE_7:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b1;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		ONE_8:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b1;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		ONE_9:
	   begin
	 key_1 = 1'b1;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	   end	
		ONE_10:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b1;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		ONE_11:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b1;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end	
		ONE_12:
	   begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b1;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	   end	
		ONE_13:
	   begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b1;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	   end	
		ONE_14:
	   begin
	 key_1 = 1'b0;
	 key_2 = 1'b1;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	   end
	
		ONE_15:
	   begin 
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b1;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
	   end	
////////////////////////////////////////////
		
	ONE_16:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b1;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		
	ONE_17:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b1;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	ONE_18:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b1;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end	
	ONE_19:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b1;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	ONE_20:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b1;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	 ONE_21:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b1;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	ONE_22:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b1;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_23:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b1;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_24:
		begin
	 key_1 = 1'b1;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		
	
	ONE_25:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b1;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	ONE_26:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b1;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	
	 ONE_27:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b1;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	

	 ONE_28:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b1;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		
	ONE_29:
		begin
	 key_1 = 1'b1;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
		
	ONE_30:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b1;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end	
		
		
		
		default:
		begin
	 key_1 = 1'b0;
	 key_2 = 1'b0;
	 key_3 = 1'b0;
	 key_4 = 1'b0;
	 key_5 = 1'b0;
	 key_6 = 1'b0;
	 key_7 = 1'b0;
	 key_8 = 1'b0;
	 key_9 = 1'b0;
	 key_10 = 1'b0;
	 key_11 = 1'b0;
	 key_12 = 1'b0;
	 key_13 = 1'b0;
	 key_14 = 1'b0;
	 key_15 = 1'b0;
	 key_16 = 1'b0;
		end
	endcase
end

	 
endmodule
