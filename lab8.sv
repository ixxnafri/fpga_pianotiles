module lab8( input               CLOCK_50,
             input        [3:0]  KEY,          //bit 0 is set up as Reset
				 input [7:0]   SW,
             output logic [6:0]  HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
             // VGA Interface 
             output logic [7:0]  VGA_R,        //VGA Red
                                 VGA_G,        //VGA Green
                                 VGA_B,        //VGA Blue
             output logic        VGA_CLK,      //VGA Clock
                                 VGA_SYNC_N,   //VGA Sync signal
                                 VGA_BLANK_N,  //VGA Blank signal
                                 VGA_VS,       //VGA virtical sync signal
                                 VGA_HS,       //VGA horizontal sync signal
             // CY7C67200 Interface
             inout  wire  [15:0] OTG_DATA,     //CY7C67200 Data bus 16 Bits
             output logic [1:0]  OTG_ADDR,     //CY7C67200 Address 2 Bits
             output logic        OTG_CS_N,     //CY7C67200 Chip Select
                                 OTG_RD_N,     //CY7C67200 Write
                                 OTG_WR_N,     //CY7C67200 Read
                                 OTG_RST_N,    //CY7C67200 Reset
             input               OTG_INT,      //CY7C67200 Interrupt
             // SDRAM Interface for Nios II Software
             output logic [12:0] DRAM_ADDR,    //SDRAM Address 13 Bits
             inout  wire  [31:0] DRAM_DQ,      //SDRAM Data 32 Bits
             output logic [1:0]  DRAM_BA,      //SDRAM Bank Address 2 Bits
             output logic [3:0]  DRAM_DQM,     //SDRAM Data Mast 4 Bits
             output logic        DRAM_RAS_N,   //SDRAM Row Address Strobe
                                 DRAM_CAS_N,   //SDRAM Column Address Strobe
                                 DRAM_CKE,     //SDRAM Clock Enable
                                 DRAM_WE_N,    //SDRAM Write Enable
                                 DRAM_CS_N,    //SDRAM Chip Select
                                 DRAM_CLK,      //SDRAM Clock
											
				//AUDIO CODEC interface
				output	logic	AUD_MCLK,
				inout		wire	AUD_BCLK,
				output	logic AUD_DACDAT,
				inout 	wire	AUD_DACLRCK,
				input				AUD_ADCDAT,
				inout 	wire	AUD_ADCLRCK,
				output	logic I2C_SDAT,
									I2C_SCLK
									
                    );
    
    logic Reset_h, Clk;
 /// Shit we need to inculde   
	 logic [15:0] keycode, keycode_2, keycode_3;
    logic [9:0] DrawX, DrawY;
	 logic  is_ball_1, is_ball_2, is_ball_3,is_ball_4, is_ball_5,is_ball_6,is_ball_7,is_ball_8,
				is_ball_9, is_ball_10, is_ball_11,is_ball_12, is_ball_13,is_ball_14,is_ball_15,is_ball_16; 
	 
    assign Clk = CLOCK_50;
    always_ff @ (posedge Clk) begin
        Reset_h <= ~(KEY[0]);        // The push buttons are active low
		  
    end
	 
    logic active_a, active_s, active_d, active_f, active_j, active_k, active_l, active_colon;
    logic [1:0] hpi_addr;
    logic [15:0] hpi_data_in, hpi_data_out;
    logic hpi_r, hpi_w, hpi_cs, hpi_reset;
    
    // Interface between NIOS II and EZ-OTG chip
    hpi_io_intf hpi_io_inst(
                            .Clk(Clk),
                            .Reset(Reset_h),
                            // signals connected to NIOS II
                            .from_sw_address(hpi_addr),
                            .from_sw_data_in(hpi_data_in),
                            .from_sw_data_out(hpi_data_out),
                            .from_sw_r(hpi_r),
                            .from_sw_w(hpi_w),
                            .from_sw_cs(hpi_cs),
                            .from_sw_reset(hpi_reset),
                            // signals connected to EZ-OTG chip
                            .OTG_DATA(OTG_DATA),    
                            .OTG_ADDR(OTG_ADDR),    
                            .OTG_RD_N(OTG_RD_N),    
                            .OTG_WR_N(OTG_WR_N),    
                            .OTG_CS_N(OTG_CS_N),
                            .OTG_RST_N(OTG_RST_N)
    );
     
     // You need to make sure that the port names here match the ports in Qsys-generated codes.
     nios_system nios_system(
                             .clk_clk(Clk),         
                             .reset_reset_n(1'b1),    // Never reset NIOS
                             .sdram_wire_addr(DRAM_ADDR), 
                             .sdram_wire_ba(DRAM_BA),   
                             .sdram_wire_cas_n(DRAM_CAS_N),
                             .sdram_wire_cke(DRAM_CKE),  
                             .sdram_wire_cs_n(DRAM_CS_N), 
                             .sdram_wire_dq(DRAM_DQ),   
                             .sdram_wire_dqm(DRAM_DQM),  
                             .sdram_wire_ras_n(DRAM_RAS_N),
                             .sdram_wire_we_n(DRAM_WE_N), 
                             .sdram_clk_clk(DRAM_CLK),
                             .keycode_export(keycode), 
									  .keycode_2_export(keycode_2),  
									  .keycode_3_export(keycode_3),	  
                             .otg_hpi_address_export(hpi_addr),
                             .otg_hpi_data_in_port(hpi_data_in),
                             .otg_hpi_data_out_port(hpi_data_out),
                             .otg_hpi_cs_export(hpi_cs),
                             .otg_hpi_r_export(hpi_r),
                             .otg_hpi_w_export(hpi_w),
                             .otg_hpi_reset_export(hpi_reset)
    );
    
    // Use PLL to generate the 25MHZ VGA_CLK.
    // You will have to generate it on your own in simulation.
    vga_clk vga_clk_instance(							.inclk0(Clk), 
																.c0(VGA_CLK));
    
    // TODO: Fill in the connections for the rest of the modules 
    VGA_controller vga_controller_instance(		.Clk(Clk),
																.Reset(Reset_h),
																.*);
																
	logic 	key_1,key_2,key_3,key_4,key_5,key_6,key_7,key_8,key_9,key_10,key_11,key_12,key_13,key_14,key_15,key_16;

	
drop_mux test(	.Clk(Clk),
					.Reset(Reset_h),
					.frame_clk(VGA_VS), 
					.SW(SW_CLOCK),
					.key_1(key_1),
					.key_2(key_2),
					.key_3(key_3),
					.key_4(key_4),
					.key_5(key_5),
					.key_6(key_6),
					.key_7(key_7),
					.key_8(key_8),
					.key_9(key_9),
					.key_10(key_10),
					.key_11(key_11),
					.key_12(key_12),
					.key_13(key_13),
					.key_14(key_14),
					.key_15(key_15),
					.key_16(key_16));	

logic active_enter;
logic ack_enter;
				



press_key  A_KEY(.actual(8'h04),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_a));
			 
press_key  S_KEY(.actual(8'h16),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_s));

press_key  D_KEY(.actual(8'h07),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_d));
			 
press_key  F_KEY(.actual(8'h09),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_f));

press_key J_KEY(.actual(8'h0d),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_j));
			 
press_key K_KEY(.actual(8'h0e),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_k));
			 
press_key L_KEY(.actual(8'h0f),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_l));
			 
press_key COLON_KEY(.actual(8'h33),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_colon));
				
press_key  ENTER_KEY(.actual(8'h28),
			 .keycode0(keycode[7:0]),
			 .keycode1(keycode[15:8]),
			 .keycode2(keycode_2[7:0]),
			 .keycode3(keycode_2[15:8]),
			 .keycode4(keycode_3[7:0]),
			 .keycode5(keycode_3[15:8]),
			 .press(active_enter));
		


		
always_comb
begin
if	(ack_enter == 1'b1)
	begin
		VGA_R = VGA_R2;
		VGA_G = VGA_G2;
		VGA_B = VGA_B2;
	end
else
	begin
		VGA_R = VGA_R1;
		VGA_G = VGA_G1;
		VGA_B = VGA_B1;
	end
end


    
    // Which signal should be frame_clk?
    Box1 ball_instance1(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_1),
																.*);
    
	Box2 ball_instance2(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_2),
																.*);
																
	Box3 ball_instance3(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_3),
																.*);
	
	Box4 ball_instance4(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_4),
																.*);
    
	Box5 ball_instance5(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_5),
																.*);
																
	
	Box6 ball_instance6(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_6),
																.*);
																
	Box7 ball_instance7(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_7),
																.*);
    
	Box8 ball_instance8(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_8),
																.*);
																
	Box9 ball_instance9(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_9),
																.*);
    
	Box10 ball_instance10(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_10),
																.*);
																
	
	Box11 ball_instance11(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_11),
																.*);
	
	Box12 ball_instance12(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_12),
																.*);
    
	Box13 ball_instance13(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_13),
																.*);
																
	
	Box14 ball_instance14(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_14),
																.*);
																
	Box15 ball_instance15(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_15),
																.*);
    
	Box16 ball_instance16(									.Clk(Clk),	
																.Reset(Reset_h),
																.frame_clk(VGA_VS),
																.key(key_16),
																.*);
	
																
    color_mapper color_instance(.*);
	 
	 
	 logic [3:0]counter;
	 	logic KEY1_PRESS,KEY2_PRESS,KEY3_PRESS;
		logic [7:0] SW_CLOCK;
	
   always_ff @ (posedge Clk) 
	begin
      KEY1_PRESS <= ~(KEY[1]);   
		KEY2_PRESS <= ~(KEY[2]); 
		KEY3_PRESS <= ~(KEY[3]); 
		SW_CLOCK = SW;
    end
	 
	 
	   always_ff @ (posedge Clk) 
	begin
		if(Reset_h)
			ack_enter = 1'b0;
		if(active_enter)
			ack_enter = 1'b1;
    end
	 
	 numcounter hexcounter(	
					.Clk(Clk),
					.KEY1(KEY1_PRESS),
					.KEY2(KEY2_PRESS),
					.KEY3(KEY3_PRESS), 
					.counter(counter[3:0])
						);
    
    // Display keycode on hex display 
    HexDriver hex_inst_0 (counter[3:0], HEX0);
    HexLetter hex_inst_1 (counter[3:0], HEX1);
	 HexDriver hex_inst_2 (keycode[11:8], HEX2);
    HexDriver hex_inst_3 (keycode[15:12], HEX3);
	 HexDriver hex_inst_4 (keycode_2[3:0], HEX4);
    HexDriver hex_inst_5 (keycode_2[7:4], HEX5);
	 HexDriver hex_inst_6 (SW[3:0], HEX6);
    HexDriver hex_inst_7 ({3'b000, ack_enter}, HEX7);
    

	 logic INIT;
	 logic[15:0] data;
	 logic init_finish;
	 logic[31:0] ADCDATA;
	 logic data_over;
	 
	 
	 ///

				
audio_interface audio0(
							  .clk(Clk), 
							  .Reset(~KEY[0]), 
							  .INIT(INIT), 
							  .INIT_FINISH(init_finish), 				
							  .adc_full(adc_full), 
							  .data_over(data_over), 
							  .AUD_MCLK(AUD_MCLK),
							  .AUD_BCLK(AUD_BCLK), 
							  .AUD_ADCDAT(AUD_ADCDAT),
							  .AUD_DACDAT(AUD_DACDAT),
							  .AUD_DACLRCK(AUD_DACLRCK),
							  .AUD_ADCLRCK(AUD_ADCLRCK),
							  .I2C_SDAT(I2C_SDAT),
							  .I2C_SCLK(I2C_SCLK),
							  .ADCDATA(ADCDATA),
							  .LDATA(data), 
							  .RDATA(data));
							  
audio_controller  audio_c(.init(INIT),
										.clk(Clk),
										.reset_n(~KEY[0]),
										.start(~KEY[1]),
										.init_done(init_finish),
										.data_over(data_over),
										.counter(counter[3:0]),
										.data_out(data),
										.active_a(active_a), 
										.active_s(active_s), 
										.active_d(active_d), 
										.active_f(active_f), 
										.active_j(active_j), 
										.active_k(active_k), 
										.active_l(active_l), 
										.active_colon(active_colon));

logic [18:0] read_address;
logic [4:0] data_Out;
logic [7:0] VGA_R1, VGA_G1, VGA_B1;
logic [7:0] VGA_R2, VGA_G2, VGA_B2;


color_mapper_sprite cmp( 
                       .DrawX(DrawX), 
							  .DrawY(DrawY),       
							  .data_Out(data_Out),
                       .VGA_R1(VGA_R1), 
							  .VGA_G1(VGA_G1), 
							  .VGA_B1(VGA_B1) 
                     );


frameRAM frame( 	.data_In(),
						.write_address(), 
						.read_address(read_address),
						.we(), 
						.Clk(Clk),
						.data_Out(data_Out)
					);

sprite mcSprito( .Clk(Clk),               
                 .Reset(Reset),              
                 .frame_clk(VGA_VS),         
					  .DrawX(DrawX), 
					  .DrawY(DrawY),       
					  .read_address(read_address)
              );

	 
endmodule
