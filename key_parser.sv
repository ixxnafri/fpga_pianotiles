module key_parser(
						input logic[7:0] compare,
						input logic[7:0] keycode70,
						input logic[7:0] keycode158,
						input logic[7:0] keycode_270,
						input logic[7:0] keycode_2158,
						input logic[7:0] keycode_370,
						input logic[7:0] keycode_3158,
						output logic key);
					 

	always_comb begin
		if(compare == keycode70)
			key = 1'b1;
		else if(compare == keycode158)
			key = 1'b1;
		else if(compare == keycode_270)
			key = 1'b1;
		else if(compare == keycode_2158)
			key = 1'b1;
		else if(compare == keycode_370)
			key = 1'b1;
		else if(compare == keycode_3158)
			key = 1'b1;
		else 
		begin
			key = 1'b0;
		end 
	end
endmodule
