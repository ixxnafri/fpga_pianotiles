module press_key(input logic[7:0] actual,
			       input logic[7:0] keycode0,
					 input logic[7:0] keycode1,
					 input logic[7:0] keycode2,
					 input logic[7:0] keycode3,
					 input logic[7:0] keycode4,
					 input logic[7:0] keycode5,
					 output logic press);
					 

	always_comb begin
		if(actual == keycode0)
			press = 1'b1;
		else if(actual == keycode1)
			press = 1'b1;
		else if(actual == keycode2)
			press = 1'b1;
		else if(actual == keycode3)
			press = 1'b1;
		else if(actual == keycode4)
			press = 1'b1;
		else if(actual == keycode4)
			press = 1'b1;
		else if(actual == keycode5)
			press = 1'b1;
		else begin
			press = 1'b0;
		end
	end
endmodule
