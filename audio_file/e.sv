module E_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
		 12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd28  	 ;
   	 12'd2  	 : out = 12'd56  	 ;
   	 12'd3  	 : out = 12'd84  	 ;
   	 12'd4  	 : out = 12'd112  	 ;
   	 12'd5  	 : out = 12'd140  	 ;
   	 12'd6  	 : out = 12'd168  	 ;
   	 12'd7  	 : out = 12'd196  	 ;
   	 12'd8  	 : out = 12'd225  	 ;
   	 12'd9  	 : out = 12'd253  	 ;
   	 12'd10  	 : out = 12'd281  	 ;
   	 12'd11  	 : out = 12'd309  	 ;
   	 12'd12  	 : out = 12'd337  	 ;
   	 12'd13  	 : out = 12'd365  	 ;
   	 12'd14  	 : out = 12'd393  	 ;
   	 12'd15  	 : out = 12'd421  	 ;
   	 12'd16  	 : out = 12'd450  	 ;
   	 12'd17  	 : out = 12'd478  	 ;
   	 12'd18  	 : out = 12'd506  	 ;
   	 12'd19  	 : out = 12'd534  	 ;
   	 12'd20  	 : out = 12'd562  	 ;
   	 12'd21  	 : out = 12'd590  	 ;
   	 12'd22  	 : out = 12'd618  	 ;
   	 12'd23  	 : out = 12'd646  	 ;
   	 12'd24  	 : out = 12'd675  	 ;
   	 12'd25  	 : out = 12'd703  	 ;
   	 12'd26  	 : out = 12'd731  	 ;
   	 12'd27  	 : out = 12'd759  	 ;
   	 12'd28  	 : out = 12'd787  	 ;
   	 12'd29  	 : out = 12'd815  	 ;
   	 12'd30  	 : out = 12'd843  	 ;
   	 12'd31  	 : out = 12'd871  	 ;
   	 12'd32  	 : out = 12'd900  	 ;
   	 12'd33  	 : out = 12'd928  	 ;
   	 12'd34  	 : out = 12'd956  	 ;
   	 12'd35  	 : out = 12'd984  	 ;
   	 12'd36  	 : out = 12'd1012  	 ;
   	 12'd37  	 : out = 12'd1040  	 ;
   	 12'd38  	 : out = 12'd1068  	 ;
   	 12'd39  	 : out = 12'd1097  	 ;
   	 12'd40  	 : out = 12'd1125  	 ;
   	 12'd41  	 : out = 12'd1153  	 ;
   	 12'd42  	 : out = 12'd1181  	 ;
   	 12'd43  	 : out = 12'd1209  	 ;
   	 12'd44  	 : out = 12'd1237  	 ;
   	 12'd45  	 : out = 12'd1265  	 ;
   	 12'd46  	 : out = 12'd1293  	 ;
   	 12'd47  	 : out = 12'd1322  	 ;
   	 12'd48  	 : out = 12'd1350  	 ;
   	 12'd49  	 : out = 12'd1378  	 ;
   	 12'd50  	 : out = 12'd1406  	 ;
   	 12'd51  	 : out = 12'd1434  	 ;
   	 12'd52  	 : out = 12'd1462  	 ;
   	 12'd53  	 : out = 12'd1490  	 ;
   	 12'd54  	 : out = 12'd1518  	 ;
   	 12'd55  	 : out = 12'd1547  	 ;
   	 12'd56  	 : out = 12'd1575  	 ;
   	 12'd57  	 : out = 12'd1603  	 ;
   	 12'd58  	 : out = 12'd1631  	 ;
   	 12'd59  	 : out = 12'd1659  	 ;
   	 12'd60  	 : out = 12'd1687  	 ;
   	 12'd61  	 : out = 12'd1715  	 ;
   	 12'd62  	 : out = 12'd1743  	 ;
   	 12'd63  	 : out = 12'd1772  	 ;
   	 12'd64  	 : out = 12'd1800  	 ;
   	 12'd65  	 : out = 12'd1828  	 ;
   	 12'd66  	 : out = 12'd1856  	 ;
   	 12'd67  	 : out = 12'd1884  	 ;
   	 12'd68  	 : out = 12'd1912  	 ;
   	 12'd69  	 : out = 12'd1940  	 ;
   	 12'd70  	 : out = 12'd1968  	 ;
   	 12'd71  	 : out = 12'd1997  	 ;
   	 12'd72  	 : out = 12'd2025  	 ;
   	 12'd73  	 : out = 12'd2053  	 ;
   	 12'd74  	 : out = 12'd2081  	 ;
   	 12'd75  	 : out = 12'd2109  	 ;
   	 12'd76  	 : out = 12'd2137  	 ;
   	 12'd77  	 : out = 12'd2165  	 ;
   	 12'd78  	 : out = 12'd2194  	 ;
   	 12'd79  	 : out = 12'd2222  	 ;
   	 12'd80  	 : out = 12'd2250  	 ;
   	 12'd81  	 : out = 12'd2278  	 ;
   	 12'd82  	 : out = 12'd2306  	 ;
   	 12'd83  	 : out = 12'd2334  	 ;
   	 12'd84  	 : out = 12'd2362  	 ;
   	 12'd85  	 : out = 12'd2390  	 ;
   	 12'd86  	 : out = 12'd2419  	 ;
   	 12'd87  	 : out = 12'd2447  	 ;
   	 12'd88  	 : out = 12'd2475  	 ;
   	 12'd89  	 : out = 12'd2503  	 ;
   	 12'd90  	 : out = 12'd2531  	 ;
   	 12'd91  	 : out = 12'd2559  	 ;
   	 12'd92  	 : out = 12'd2587  	 ;
   	 12'd93  	 : out = 12'd2615  	 ;
   	 12'd94  	 : out = 12'd2644  	 ;
   	 12'd95  	 : out = 12'd2672  	 ;
   	 12'd96  	 : out = 12'd2700  	 ;
   	 12'd97  	 : out = 12'd2728  	 ;
   	 12'd98  	 : out = 12'd2756  	 ;
   	 12'd99  	 : out = 12'd2784  	 ;
   	 12'd100  	 : out = 12'd2812  	 ;
   	 12'd101  	 : out = 12'd2840  	 ;
   	 12'd102  	 : out = 12'd2869  	 ;
   	 12'd103  	 : out = 12'd2897  	 ;
   	 12'd104  	 : out = 12'd2925  	 ;
   	 12'd105  	 : out = 12'd2953  	 ;
   	 12'd106  	 : out = 12'd2981  	 ;
   	 12'd107  	 : out = 12'd3009  	 ;
   	 12'd108  	 : out = 12'd3037  	 ;
   	 12'd109  	 : out = 12'd3065  	 ;
   	 12'd110  	 : out = 12'd3094  	 ;
   	 12'd111  	 : out = 12'd3122  	 ;
   	 12'd112  	 : out = 12'd3150  	 ;
   	 12'd113  	 : out = 12'd3178  	 ;
   	 12'd114  	 : out = 12'd3206  	 ;
   	 12'd115  	 : out = 12'd3234  	 ;
   	 12'd116  	 : out = 12'd3262  	 ;
   	 12'd117  	 : out = 12'd3291  	 ;
   	 12'd118  	 : out = 12'd3319  	 ;
   	 12'd119  	 : out = 12'd3347  	 ;
   	 12'd120  	 : out = 12'd3375  	 ;
   	 12'd121  	 : out = 12'd3403  	 ;
   	 12'd122  	 : out = 12'd3431  	 ;
   	 12'd123  	 : out = 12'd3459  	 ;
   	 12'd124  	 : out = 12'd3487  	 ;
   	 12'd125  	 : out = 12'd3516  	 ;
   	 12'd126  	 : out = 12'd3544  	 ;
   	 12'd127  	 : out = 12'd3572  	 ;
   	 12'd128  	 : out = 12'd3600  	 ;
   	 12'd129  	 : out = 12'd3628  	 ;
   	 12'd130  	 : out = 12'd3656  	 ;
   	 12'd131  	 : out = 12'd3684  	 ;
   	 12'd132  	 : out = 12'd3712  	 ;
   	 12'd133  	 : out = 12'd3741  	 ;
   	 12'd134  	 : out = 12'd3769  	 ;
   	 12'd135  	 : out = 12'd3797  	 ;
   	 12'd136  	 : out = 12'd3825  	 ;
   	 12'd137  	 : out = 12'd3853  	 ;
   	 12'd138  	 : out = 12'd3881  	 ;
   	 12'd139  	 : out = 12'd3909  	 ;
   	 12'd140  	 : out = 12'd3937  	 ;
   	 12'd141  	 : out = 12'd3966  	 ;
   	 12'd142  	 : out = 12'd3994  	 ;
   	 12'd143  	 : out = 12'd4022  	 ;
   	 12'd144  	 : out = 12'd4050  	 ;
   	 12'd145  	 : out = 12'd4078  	 ;

		default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule

module E_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
	12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd56  	 ;
   	 12'd2  	 : out = 12'd112  	 ;
   	 12'd3  	 : out = 12'd168  	 ;
   	 12'd4  	 : out = 12'd225  	 ;
   	 12'd5  	 : out = 12'd281  	 ;
   	 12'd6  	 : out = 12'd337  	 ;
   	 12'd7  	 : out = 12'd393  	 ;
   	 12'd8  	 : out = 12'd450  	 ;
   	 12'd9  	 : out = 12'd506  	 ;
   	 12'd10  	 : out = 12'd562  	 ;
   	 12'd11  	 : out = 12'd618  	 ;
   	 12'd12  	 : out = 12'd675  	 ;
   	 12'd13  	 : out = 12'd731  	 ;
   	 12'd14  	 : out = 12'd787  	 ;
   	 12'd15  	 : out = 12'd843  	 ;
   	 12'd16  	 : out = 12'd900  	 ;
   	 12'd17  	 : out = 12'd956  	 ;
   	 12'd18  	 : out = 12'd1012  	 ;
   	 12'd19  	 : out = 12'd1068  	 ;
   	 12'd20  	 : out = 12'd1125  	 ;
   	 12'd21  	 : out = 12'd1181  	 ;
   	 12'd22  	 : out = 12'd1237  	 ;
   	 12'd23  	 : out = 12'd1293  	 ;
   	 12'd24  	 : out = 12'd1350  	 ;
   	 12'd25  	 : out = 12'd1406  	 ;
   	 12'd26  	 : out = 12'd1462  	 ;
   	 12'd27  	 : out = 12'd1518  	 ;
   	 12'd28  	 : out = 12'd1575  	 ;
   	 12'd29  	 : out = 12'd1631  	 ;
   	 12'd30  	 : out = 12'd1687  	 ;
   	 12'd31  	 : out = 12'd1743  	 ;
   	 12'd32  	 : out = 12'd1800  	 ;
   	 12'd33  	 : out = 12'd1856  	 ;
   	 12'd34  	 : out = 12'd1912  	 ;
   	 12'd35  	 : out = 12'd1968  	 ;
   	 12'd36  	 : out = 12'd2025  	 ;
   	 12'd37  	 : out = 12'd2081  	 ;
   	 12'd38  	 : out = 12'd2137  	 ;
   	 12'd39  	 : out = 12'd2193  	 ;
   	 12'd40  	 : out = 12'd2250  	 ;
   	 12'd41  	 : out = 12'd2306  	 ;
   	 12'd42  	 : out = 12'd2362  	 ;
   	 12'd43  	 : out = 12'd2419  	 ;
   	 12'd44  	 : out = 12'd2475  	 ;
   	 12'd45  	 : out = 12'd2531  	 ;
   	 12'd46  	 : out = 12'd2587  	 ;
   	 12'd47  	 : out = 12'd2644  	 ;
   	 12'd48  	 : out = 12'd2700  	 ;
   	 12'd49  	 : out = 12'd2756  	 ;
   	 12'd50  	 : out = 12'd2812  	 ;
   	 12'd51  	 : out = 12'd2869  	 ;
   	 12'd52  	 : out = 12'd2925  	 ;
   	 12'd53  	 : out = 12'd2981  	 ;
   	 12'd54  	 : out = 12'd3037  	 ;
   	 12'd55  	 : out = 12'd3094  	 ;
   	 12'd56  	 : out = 12'd3150  	 ;
   	 12'd57  	 : out = 12'd3206  	 ;
   	 12'd58  	 : out = 12'd3262  	 ;
   	 12'd59  	 : out = 12'd3319  	 ;
   	 12'd60  	 : out = 12'd3375  	 ;
   	 12'd61  	 : out = 12'd3431  	 ;
   	 12'd62  	 : out = 12'd3487  	 ;
   	 12'd63  	 : out = 12'd3544  	 ;
   	 12'd64  	 : out = 12'd3600  	 ;
   	 12'd65  	 : out = 12'd3656  	 ;
   	 12'd66  	 : out = 12'd3712  	 ;
   	 12'd67  	 : out = 12'd3769  	 ;
   	 12'd68  	 : out = 12'd3825  	 ;
   	 12'd69  	 : out = 12'd3881  	 ;
   	 12'd70  	 : out = 12'd3937  	 ;
   	 12'd71  	 : out = 12'd3994  	 ;
   	 12'd72  	 : out = 12'd4050  	 ;


		default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule
