module wave_generator(
                      input                  clk,
                      input                  reset_n,  // Active-low reset
							 input logic 				key_ready,
							 input  logic[3:0]      tone,
							 input  logic[12:0] 		sample_num,
							 input logic init_done,
							 input logic data_over,
							 output logic [15:0] data_out
       );

enum logic [3:0] {IDLE, GET_KEY, LOAD_TONE, PLAY,DONE_PLAY,ADSR_UPDATE, CHECK_COUNT} state, next_state;
logic [15:0] 	DATA;
logic [11:0] counter, counter_in;
logic [15:0] c_4,d_4,e_4,f_4,g_4,a_4,b_4,c_5,d_5,e_5,f_5,g_5,a_5,b_5,c_6;
logic [3:0] tone_out;
logic Load;
assign data_out = DATA;

//Things for adsr
logic[2:0] adsr_state, adsr_state_in;
logic key_release, att_done, rel_done, dec_done, sus_done;
logic[15:0] adsr_sample, adsr_sample_in;
logic[31:0] adsr_fp, adsr_fp_in, adsr_fp_out;


reg_3 register(	.Clk(clk),
						.Reset(~reset_n),
						.Load(Load),
						.D(tone),
						.Data_Out(tone_out)
					);

// This instantiation is for week 1. For week 2, instantiate AES according to new interfaces.

C_4 	c4(.in(counter),.dval(c_4));
D_4 	d4(.in(counter),.dval(d_4));
E_4 	e4(.in(counter),.dval(e_4));
F_4 	f4(.in(counter),.dval(f_4));
G_4 	g4(.in(counter),.dval(g_4));
A_4	a4(.in(counter),.dval(a_4));
B_4	b4(.in(counter),.dval(b_4));
C_5 	c5(.in(counter),.dval(c_5));
D_5 	d5(.in(counter),.dval(d_5));
E_5 	e5(.in(counter),.dval(e_5));
F_5 	f5(.in(counter),.dval(f_5));
G_5 	g5(.in(counter),.dval(g_5));
A_5 	a5(.in(counter),.dval(a_5));
B_5 	b5(.in(counter),.dval(b_5));
C_6 	c6(.in(counter),.dval(c_6));

adsr_controller adsr_0 (.adsr_state(adsr_state),
								.adsr_fp(adsr_fp),
								.adsr_fp_out(adsr_fp_out),
								.sample(adsr_sample_in),
								.key_release(~key_ready),
								.sample_out(DATA),
								.att_done(att_done),.dec_done(dec_done),.sus_done(sus_done),.rel_done(rel_done));

tone_checker master_tone(
					.c_4(c_4),
					.d_4(d_4),
					.e_4(e_4),
					.f_4(f_4),
					.g_4(g_4),
					.a_4(a_4),
					.b_4(b_4),
					.c_5(c_5),
					.d_5(d_5),
					.e_5(e_5),
					.f_5(f_5),
					.g_5(g_5),
					.a_5(a_5),
					.b_5(b_5),
					.c_6(c_6),
					.select(tone_out),
					.out(adsr_sample_in));
							  

always_ff @ (posedge clk) begin
    if (~reset_n) begin
		  counter = 12'b0;
        state <= IDLE;
		  adsr_state <= 3'b000;
		  adsr_fp <= 32'd0;
    end 
    else begin
		  counter <= counter_in;
        state <= next_state; 
		  adsr_fp <= adsr_fp_in;
		  adsr_state <= adsr_state_in;
    end
end

always_comb begin

    next_state = state;
    case (state)
        // Wait until IO is ready.
        IDLE: begin
            if (init_done)
                next_state = GET_KEY;
        end
			GET_KEY: begin	
				if(key_ready)
					next_state = LOAD_TONE;
			end
        LOAD_TONE: begin
             next_state = PLAY;
        end
		  PLAY: begin
					next_state = DONE_PLAY;
			end
		  DONE_PLAY:begin
					if(data_over)
						next_state = ADSR_UPDATE;
		  end
		  //wait for sample process to run
		  ADSR_UPDATE: begin
				if(rel_done) begin
					next_state = GET_KEY;
				end
				else begin
					next_state = PLAY;
				end
			end
        CHECK_COUNT: begin
				next_state = PLAY;
        end
    endcase
	 
    // Control signals
	 counter_in = counter;
	 Load = 1'b0;
	 adsr_state_in = adsr_state;
	 adsr_fp_in = adsr_fp;
    case (state)
        IDLE: begin
	
        end
		  GET_KEY: begin
			  counter_in = 12'b0;
			  adsr_fp_in = 32'd0;
			  adsr_state_in = 3'b000;
	
		  end 
		  LOAD_TONE: begin
				Load = 1'b1;

        end
		  PLAY: begin
				Load = 1'b0;
				counter_in = counter;
		
		  end
        DONE_PLAY:begin

		  end
		  ADSR_UPDATE: begin
				if(att_done) begin
					adsr_state_in = 3'b001;
				end
				else if(dec_done) begin 
					adsr_state_in = 3'b010;
				end
				else if(sus_done) begin
					adsr_state_in = 3'b011;
				end
				adsr_fp_in = adsr_fp_out;
				if(counter != sample_num) begin
					counter_in = counter + 1'b1;	
				end	
				else if(counter == sample_num) begin
					counter_in = 12'd0;
				end
			end
			default:;
    endcase
end
     
endmodule
