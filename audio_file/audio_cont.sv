module audio_controller (output logic init,
							    input logic clk,
								 input logic reset_n,
								 input logic start,
								 input logic init_done,
								 input logic data_over,
								 input logic active_a, active_s, active_d, active_f, active_j, active_k, active_l, active_colon,
								 input logic[3:0] counter, 
								 output logic[15:0] data_out);
								 
								 

enum logic [2:0] {IDLE, INIT_START, INIT_WAIT, PLAY} state, next_state;

logic key_c4, key_c5, key_c6, key_d4, key_d5, key_e4, key_e5, key_f4, key_f5, key_g4, key_g5, 
		key_a4, key_a5, key_b4, key_b5;
		
logic[15:0] sample_c4, sample_d4, sample_e4, sample_f4, sample_g4, sample_a4, sample_b4, sample_c5, sample_d5, sample_e5, sample_f5, sample_g5, sample_a5, sample_b5, sample_c6, 
out1,out2,out3,out4,out5,out6,out7,out8,
sample_cd, sample_ef, sample_ga, sample_bc, sample_cdef, sample_gabc;


			 
wave_generator sine_c4(.clk(clk),
							  .reset_n(~reset_n),  
							  .key_ready(key_c4),
							  .tone(4'd0),
							  .sample_num(12'd184),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_c4));			 
			 
wave_generator sine_d4(.clk(clk),
							  .reset_n(~reset_n),  
							  .key_ready(key_d4),
							  .tone(4'd1),
							  .sample_num(12'd164),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_d4));
wave_generator sine_e4(.clk(clk),
							  .reset_n(~reset_n),  
							  .key_ready(key_e4),
							  .tone(4'd2),
							  .sample_num(12'd146),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_e4));
wave_generator sine_f4(.clk(clk),
							  .reset_n(~reset_n), 
							  .key_ready(key_f4),
							  .tone(4'd3),
							  .sample_num(12'd138),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_f4));
wave_generator sine_g4(.clk(clk),
							  .reset_n(~reset_n),  
							  .key_ready(key_g4),
							  .tone(4'd4),
							  .sample_num(12'd123),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_g4));
							  
wave_generator sine_a4(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_a4),
							  .tone(4'd5),
							  .sample_num(12'd110),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_a4));

wave_generator sine_b4(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_b4),
							  .tone(4'd6),
							  .sample_num(12'd98),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_b4));

wave_generator sine_c5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_c5),
							  .tone(4'd7),
							  .sample_num(10'd92),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_c5));
/////////////////////////////////////////////////////////							  
wave_generator sine_d5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_d5),
							  .tone(4'd8),
							  .sample_num(10'd82),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_d5));
wave_generator sine_e5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_e5),
							  .tone(4'd9),
							  .sample_num(10'd73),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_e5));
wave_generator sine_f5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_f5),
							  .tone(4'd10),
							  .sample_num(10'd68),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_f5));
wave_generator sine_g5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_g5),
							  .tone(4'd11),
							  .sample_num(10'd62),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_g5));
wave_generator sine_a5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_a5),
							  .tone(4'd12),
							  .sample_num(10'd55),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_a5));
wave_generator sine_b5(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_b5),
							  .tone(4'd13),
							  .sample_num(10'd48),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_b5));
wave_generator sine_c6(.clk(clk),
							  .reset_n(~reset_n),  // Active-low reset
							  .key_ready(key_c6),
							  .tone(4'd14),
							  .sample_num(10'd45),
							  .init_done(init_done),
							  .data_over(data_over),
							  .data_out(sample_c6));

muxcounter mux1( .sample_c4(sample_c4), 
					  .sample_d4(sample_d4), 
					  .sample_e4(sample_e4),
					  .sample_f4(sample_f4), 
					  .sample_g4(sample_g4), 
					  .sample_a4(sample_a4), 
					  .sample_b4(sample_b4), 
					  .sample_c5(sample_c5), 
					  .sample_d5(sample_d5), 
					  .sample_e5(sample_e5), 
					  .sample_f5(sample_f5), 
					  .sample_g5(sample_g5), 
					  .sample_a5(sample_a5), 
					  .sample_b5(sample_b5), 
					  .sample_c6(sample_c6), 
					  .counter(counter), 
					  .out1(out1), 
					  .out2(out2),
					  .out3(out3),
					  .out4(out4),
					  .out5(out5),
					  .out6(out6),
					  .out7(out7),
					  .out8(out8),
					  .*);							  
						

addition_synthesis as1(.in1(out1),
							  .in2(out2),
							  .out(sample_cd));
							  
addition_synthesis as2(.in1(out3),
							  .in2(out4),
							  .out(sample_ef));
							  
addition_synthesis as3(.in1(out5),
							  .in2(out6),
							  .out(sample_ga));
							  
addition_synthesis as4(.in1(out7),
							  .in2(out8),
							  .out(sample_bc));
							  
addition_synthesis as5(.in1(sample_cd),
							  .in2(sample_ef),
							  .out(sample_cdef));
							  
addition_synthesis as6(.in1(sample_ga),
							  .in2(sample_bc),
							  .out(sample_gabc));
							  
addition_synthesis as7(.in1(sample_cdef),
							  .in2(sample_gabc),
							  .out(data_out));


							  

always_ff @ (posedge clk) begin
    if (~reset_n) begin
        state <= IDLE;
    end 
    else begin
        state <= next_state; 
    end
end

always_comb begin
    // Next state logic
    next_state = state;
    case (state)
		IDLE: if(start) next_state = INIT_START;
		INIT_START: next_state = INIT_WAIT;
		INIT_WAIT: if(init_done) next_state = PLAY;
		PLAY:
				next_state = PLAY;
	 endcase

	 init = 1'b0;
	 case(state)
		IDLE: begin
			init = 1'b0;
		end
		INIT_START: begin
			init = 1'b1;
		end
		INIT_WAIT: begin
			init = 1'b0;
		end
		PLAY:;
		default:;
	endcase
end

endmodule
