module reg_3 (input  logic Clk, Reset, Load,
              input  logic [3:0]  D,
              output logic [3:0]  Data_Out);

    always_ff @ (posedge Clk)
    begin
	 	 if (Reset) 
			  Data_Out <= 4'h0;
		 else if (Load)
			  Data_Out <= D;
    end
	

endmodule
