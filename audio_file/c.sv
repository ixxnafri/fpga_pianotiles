module C_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
		 12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd22  	 ;
   	 12'd2  	 : out = 12'd44  	 ;
   	 12'd3  	 : out = 12'd66  	 ;
   	 12'd4  	 : out = 12'd89  	 ;
   	 12'd5  	 : out = 12'd111  	 ;
   	 12'd6  	 : out = 12'd133  	 ;
   	 12'd7  	 : out = 12'd156  	 ;
   	 12'd8  	 : out = 12'd178  	 ;
   	 12'd9  	 : out = 12'd200  	 ;
   	 12'd10  	 : out = 12'd223  	 ;
   	 12'd11  	 : out = 12'd245  	 ;
   	 12'd12  	 : out = 12'd267  	 ;
   	 12'd13  	 : out = 12'd290  	 ;
   	 12'd14  	 : out = 12'd312  	 ;
   	 12'd15  	 : out = 12'd334  	 ;
   	 12'd16  	 : out = 12'd357  	 ;
   	 12'd17  	 : out = 12'd379  	 ;
   	 12'd18  	 : out = 12'd401  	 ;
   	 12'd19  	 : out = 12'd424  	 ;
   	 12'd20  	 : out = 12'd446  	 ;
   	 12'd21  	 : out = 12'd468  	 ;
   	 12'd22  	 : out = 12'd491  	 ;
   	 12'd23  	 : out = 12'd513  	 ;
   	 12'd24  	 : out = 12'd535  	 ;
   	 12'd25  	 : out = 12'd558  	 ;
   	 12'd26  	 : out = 12'd580  	 ;
   	 12'd27  	 : out = 12'd602  	 ;
   	 12'd28  	 : out = 12'd625  	 ;
   	 12'd29  	 : out = 12'd647  	 ;
   	 12'd30  	 : out = 12'd669  	 ;
   	 12'd31  	 : out = 12'd692  	 ;
   	 12'd32  	 : out = 12'd714  	 ;
   	 12'd33  	 : out = 12'd736  	 ;
   	 12'd34  	 : out = 12'd759  	 ;
   	 12'd35  	 : out = 12'd781  	 ;
   	 12'd36  	 : out = 12'd803  	 ;
   	 12'd37  	 : out = 12'd826  	 ;
   	 12'd38  	 : out = 12'd848  	 ;
   	 12'd39  	 : out = 12'd870  	 ;
   	 12'd40  	 : out = 12'd893  	 ;
   	 12'd41  	 : out = 12'd915  	 ;
   	 12'd42  	 : out = 12'd937  	 ;
   	 12'd43  	 : out = 12'd960  	 ;
   	 12'd44  	 : out = 12'd982  	 ;
   	 12'd45  	 : out = 12'd1004  	 ;
   	 12'd46  	 : out = 12'd1026  	 ;
   	 12'd47  	 : out = 12'd1049  	 ;
   	 12'd48  	 : out = 12'd1071  	 ;
   	 12'd49  	 : out = 12'd1093  	 ;
   	 12'd50  	 : out = 12'd1116  	 ;
   	 12'd51  	 : out = 12'd1138  	 ;
   	 12'd52  	 : out = 12'd1160  	 ;
   	 12'd53  	 : out = 12'd1183  	 ;
   	 12'd54  	 : out = 12'd1205  	 ;
   	 12'd55  	 : out = 12'd1227  	 ;
   	 12'd56  	 : out = 12'd1250  	 ;
   	 12'd57  	 : out = 12'd1272  	 ;
   	 12'd58  	 : out = 12'd1294  	 ;
   	 12'd59  	 : out = 12'd1317  	 ;
   	 12'd60  	 : out = 12'd1339  	 ;
   	 12'd61  	 : out = 12'd1361  	 ;
   	 12'd62  	 : out = 12'd1384  	 ;
   	 12'd63  	 : out = 12'd1406  	 ;
   	 12'd64  	 : out = 12'd1428  	 ;
   	 12'd65  	 : out = 12'd1451  	 ;
   	 12'd66  	 : out = 12'd1473  	 ;
   	 12'd67  	 : out = 12'd1495  	 ;
   	 12'd68  	 : out = 12'd1518  	 ;
   	 12'd69  	 : out = 12'd1540  	 ;
   	 12'd70  	 : out = 12'd1562  	 ;
   	 12'd71  	 : out = 12'd1585  	 ;
   	 12'd72  	 : out = 12'd1607  	 ;
   	 12'd73  	 : out = 12'd1629  	 ;
   	 12'd74  	 : out = 12'd1652  	 ;
   	 12'd75  	 : out = 12'd1674  	 ;
   	 12'd76  	 : out = 12'd1696  	 ;
   	 12'd77  	 : out = 12'd1719  	 ;
   	 12'd78  	 : out = 12'd1741  	 ;
   	 12'd79  	 : out = 12'd1763  	 ;
   	 12'd80  	 : out = 12'd1786  	 ;
   	 12'd81  	 : out = 12'd1808  	 ;
   	 12'd82  	 : out = 12'd1830  	 ;
   	 12'd83  	 : out = 12'd1853  	 ;
   	 12'd84  	 : out = 12'd1875  	 ;
   	 12'd85  	 : out = 12'd1897  	 ;
   	 12'd86  	 : out = 12'd1920  	 ;
   	 12'd87  	 : out = 12'd1942  	 ;
   	 12'd88  	 : out = 12'd1964  	 ;
   	 12'd89  	 : out = 12'd1986  	 ;
   	 12'd90  	 : out = 12'd2009  	 ;
   	 12'd91  	 : out = 12'd2031  	 ;
   	 12'd92  	 : out = 12'd2053  	 ;
   	 12'd93  	 : out = 12'd2076  	 ;
   	 12'd94  	 : out = 12'd2098  	 ;
   	 12'd95  	 : out = 12'd2120  	 ;
   	 12'd96  	 : out = 12'd2143  	 ;
   	 12'd97  	 : out = 12'd2165  	 ;
   	 12'd98  	 : out = 12'd2187  	 ;
   	 12'd99  	 : out = 12'd2210  	 ;
   	 12'd100  	 : out = 12'd2232  	 ;
   	 12'd101  	 : out = 12'd2254  	 ;
   	 12'd102  	 : out = 12'd2277  	 ;
   	 12'd103  	 : out = 12'd2299  	 ;
   	 12'd104  	 : out = 12'd2321  	 ;
   	 12'd105  	 : out = 12'd2344  	 ;
   	 12'd106  	 : out = 12'd2366  	 ;
   	 12'd107  	 : out = 12'd2388  	 ;
   	 12'd108  	 : out = 12'd2411  	 ;
   	 12'd109  	 : out = 12'd2433  	 ;
   	 12'd110  	 : out = 12'd2455  	 ;
   	 12'd111  	 : out = 12'd2478  	 ;
   	 12'd112  	 : out = 12'd2500  	 ;
   	 12'd113  	 : out = 12'd2522  	 ;
   	 12'd114  	 : out = 12'd2545  	 ;
   	 12'd115  	 : out = 12'd2567  	 ;
   	 12'd116  	 : out = 12'd2589  	 ;
   	 12'd117  	 : out = 12'd2612  	 ;
   	 12'd118  	 : out = 12'd2634  	 ;
   	 12'd119  	 : out = 12'd2656  	 ;
   	 12'd120  	 : out = 12'd2679  	 ;
   	 12'd121  	 : out = 12'd2701  	 ;
   	 12'd122  	 : out = 12'd2723  	 ;
   	 12'd123  	 : out = 12'd2746  	 ;
   	 12'd124  	 : out = 12'd2768  	 ;
   	 12'd125  	 : out = 12'd2790  	 ;
   	 12'd126  	 : out = 12'd2813  	 ;
   	 12'd127  	 : out = 12'd2835  	 ;
   	 12'd128  	 : out = 12'd2857  	 ;
   	 12'd129  	 : out = 12'd2880  	 ;
   	 12'd130  	 : out = 12'd2902  	 ;
   	 12'd131  	 : out = 12'd2924  	 ;
   	 12'd132  	 : out = 12'd2947  	 ;
   	 12'd133  	 : out = 12'd2969  	 ;
   	 12'd134  	 : out = 12'd2991  	 ;
   	 12'd135  	 : out = 12'd3013  	 ;
   	 12'd136  	 : out = 12'd3036  	 ;
   	 12'd137  	 : out = 12'd3058  	 ;
   	 12'd138  	 : out = 12'd3080  	 ;
   	 12'd139  	 : out = 12'd3103  	 ;
   	 12'd140  	 : out = 12'd3125  	 ;
   	 12'd141  	 : out = 12'd3147  	 ;
   	 12'd142  	 : out = 12'd3170  	 ;
   	 12'd143  	 : out = 12'd3192  	 ;
   	 12'd144  	 : out = 12'd3214  	 ;
   	 12'd145  	 : out = 12'd3237  	 ;
   	 12'd146  	 : out = 12'd3259  	 ;
   	 12'd147  	 : out = 12'd3281  	 ;
   	 12'd148  	 : out = 12'd3304  	 ;
   	 12'd149  	 : out = 12'd3326  	 ;
   	 12'd150  	 : out = 12'd3348  	 ;
   	 12'd151  	 : out = 12'd3371  	 ;
   	 12'd152  	 : out = 12'd3393  	 ;
   	 12'd153  	 : out = 12'd3415  	 ;
   	 12'd154  	 : out = 12'd3438  	 ;
   	 12'd155  	 : out = 12'd3460  	 ;
   	 12'd156  	 : out = 12'd3482  	 ;
   	 12'd157  	 : out = 12'd3505  	 ;
   	 12'd158  	 : out = 12'd3527  	 ;
   	 12'd159  	 : out = 12'd3549  	 ;
   	 12'd160  	 : out = 12'd3572  	 ;
   	 12'd161  	 : out = 12'd3594  	 ;
   	 12'd162  	 : out = 12'd3616  	 ;
   	 12'd163  	 : out = 12'd3639  	 ;
   	 12'd164  	 : out = 12'd3661  	 ;
   	 12'd165  	 : out = 12'd3683  	 ;
   	 12'd166  	 : out = 12'd3706  	 ;
   	 12'd167  	 : out = 12'd3728  	 ;
   	 12'd168  	 : out = 12'd3750  	 ;
   	 12'd169  	 : out = 12'd3773  	 ;
   	 12'd170  	 : out = 12'd3795  	 ;
   	 12'd171  	 : out = 12'd3817  	 ;
   	 12'd172  	 : out = 12'd3840  	 ;
   	 12'd173  	 : out = 12'd3862  	 ;
   	 12'd174  	 : out = 12'd3884  	 ;
   	 12'd175  	 : out = 12'd3907  	 ;
   	 12'd176  	 : out = 12'd3929  	 ;
   	 12'd177  	 : out = 12'd3951  	 ;
   	 12'd178  	 : out = 12'd3973  	 ;
   	 12'd179  	 : out = 12'd3996  	 ;
   	 12'd180  	 : out = 12'd4018  	 ;
   	 12'd181  	 : out = 12'd4040  	 ;
   	 12'd182  	 : out = 12'd4063  	 ;
   	 12'd183  	 : out = 12'd4085  	 ;


		default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule


module C_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
			
   	 12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd44  	 ;
   	 12'd2  	 : out = 12'd89  	 ;
   	 12'd3  	 : out = 12'd133  	 ;
   	 12'd4  	 : out = 12'd178  	 ;
   	 12'd5  	 : out = 12'd223  	 ;
   	 12'd6  	 : out = 12'd267  	 ;
   	 12'd7  	 : out = 12'd312  	 ;
   	 12'd8  	 : out = 12'd357  	 ;
   	 12'd9  	 : out = 12'd401  	 ;
   	 12'd10  	 : out = 12'd446  	 ;
   	 12'd11  	 : out = 12'd491  	 ;
   	 12'd12  	 : out = 12'd535  	 ;
   	 12'd13  	 : out = 12'd580  	 ;
   	 12'd14  	 : out = 12'd625  	 ;
   	 12'd15  	 : out = 12'd669  	 ;
   	 12'd16  	 : out = 12'd714  	 ;
   	 12'd17  	 : out = 12'd759  	 ;
   	 12'd18  	 : out = 12'd803  	 ;
   	 12'd19  	 : out = 12'd848  	 ;
   	 12'd20  	 : out = 12'd893  	 ;
   	 12'd21  	 : out = 12'd937  	 ;
   	 12'd22  	 : out = 12'd982  	 ;
   	 12'd23  	 : out = 12'd1026  	 ;
   	 12'd24  	 : out = 12'd1071  	 ;
   	 12'd25  	 : out = 12'd1116  	 ;
   	 12'd26  	 : out = 12'd1160  	 ;
   	 12'd27  	 : out = 12'd1205  	 ;
   	 12'd28  	 : out = 12'd1250  	 ;
   	 12'd29  	 : out = 12'd1294  	 ;
   	 12'd30  	 : out = 12'd1339  	 ;
   	 12'd31  	 : out = 12'd1384  	 ;
   	 12'd32  	 : out = 12'd1428  	 ;
   	 12'd33  	 : out = 12'd1473  	 ;
   	 12'd34  	 : out = 12'd1518  	 ;
   	 12'd35  	 : out = 12'd1562  	 ;
   	 12'd36  	 : out = 12'd1607  	 ;
   	 12'd37  	 : out = 12'd1652  	 ;
   	 12'd38  	 : out = 12'd1696  	 ;
   	 12'd39  	 : out = 12'd1741  	 ;
   	 12'd40  	 : out = 12'd1786  	 ;
   	 12'd41  	 : out = 12'd1830  	 ;
   	 12'd42  	 : out = 12'd1875  	 ;
   	 12'd43  	 : out = 12'd1919  	 ;
   	 12'd44  	 : out = 12'd1964  	 ;
   	 12'd45  	 : out = 12'd2009  	 ;
   	 12'd46  	 : out = 12'd2053  	 ;
   	 12'd47  	 : out = 12'd2098  	 ;
   	 12'd48  	 : out = 12'd2143  	 ;
   	 12'd49  	 : out = 12'd2187  	 ;
   	 12'd50  	 : out = 12'd2232  	 ;
   	 12'd51  	 : out = 12'd2277  	 ;
   	 12'd52  	 : out = 12'd2321  	 ;
   	 12'd53  	 : out = 12'd2366  	 ;
   	 12'd54  	 : out = 12'd2411  	 ;
   	 12'd55  	 : out = 12'd2455  	 ;
   	 12'd56  	 : out = 12'd2500  	 ;
   	 12'd57  	 : out = 12'd2545  	 ;
   	 12'd58  	 : out = 12'd2589  	 ;
   	 12'd59  	 : out = 12'd2634  	 ;
   	 12'd60  	 : out = 12'd2679  	 ;
   	 12'd61  	 : out = 12'd2723  	 ;
   	 12'd62  	 : out = 12'd2768  	 ;
   	 12'd63  	 : out = 12'd2812  	 ;
   	 12'd64  	 : out = 12'd2857  	 ;
   	 12'd65  	 : out = 12'd2902  	 ;
   	 12'd66  	 : out = 12'd2946  	 ;
   	 12'd67  	 : out = 12'd2991  	 ;
   	 12'd68  	 : out = 12'd3036  	 ;
   	 12'd69  	 : out = 12'd3080  	 ;
   	 12'd70  	 : out = 12'd3125  	 ;
   	 12'd71  	 : out = 12'd3170  	 ;
   	 12'd72  	 : out = 12'd3214  	 ;
   	 12'd73  	 : out = 12'd3259  	 ;
   	 12'd74  	 : out = 12'd3304  	 ;
   	 12'd75  	 : out = 12'd3348  	 ;
   	 12'd76  	 : out = 12'd3393  	 ;
   	 12'd77  	 : out = 12'd3438  	 ;
   	 12'd78  	 : out = 12'd3482  	 ;
   	 12'd79  	 : out = 12'd3527  	 ;
   	 12'd80  	 : out = 12'd3572  	 ;
   	 12'd81  	 : out = 12'd3616  	 ;
   	 12'd82  	 : out = 12'd3661  	 ;
   	 12'd83  	 : out = 12'd3706  	 ;
   	 12'd84  	 : out = 12'd3750  	 ;
   	 12'd85  	 : out = 12'd3795  	 ;
   	 12'd86  	 : out = 12'd3839  	 ;
   	 12'd87  	 : out = 12'd3884  	 ;
   	 12'd88  	 : out = 12'd3929  	 ;
   	 12'd89  	 : out = 12'd3973  	 ;
   	 12'd90  	 : out = 12'd4018  	 ;
   	 12'd91  	 : out = 12'd4063  	 ;
		default: begin
					out = 12'bZZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule


module C_6( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
		12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd89  	 ;
   	 12'd2  	 : out = 12'd178  	 ;
   	 12'd3  	 : out = 12'd267  	 ;
   	 12'd4  	 : out = 12'd357  	 ;
   	 12'd5  	 : out = 12'd446  	 ;
   	 12'd6  	 : out = 12'd535  	 ;
   	 12'd7  	 : out = 12'd625  	 ;
   	 12'd8  	 : out = 12'd714  	 ;
   	 12'd9  	 : out = 12'd803  	 ;
   	 12'd10  	 : out = 12'd893  	 ;
   	 12'd11  	 : out = 12'd982  	 ;
   	 12'd12  	 : out = 12'd1071  	 ;
   	 12'd13  	 : out = 12'd1160  	 ;
   	 12'd14  	 : out = 12'd1250  	 ;
   	 12'd15  	 : out = 12'd1339  	 ;
   	 12'd16  	 : out = 12'd1428  	 ;
   	 12'd17  	 : out = 12'd1518  	 ;
   	 12'd18  	 : out = 12'd1607  	 ;
   	 12'd19  	 : out = 12'd1696  	 ;
   	 12'd20  	 : out = 12'd1786  	 ;
   	 12'd21  	 : out = 12'd1875  	 ;
   	 12'd22  	 : out = 12'd1964  	 ;
   	 12'd23  	 : out = 12'd2053  	 ;
   	 12'd24  	 : out = 12'd2143  	 ;
   	 12'd25  	 : out = 12'd2232  	 ;
   	 12'd26  	 : out = 12'd2321  	 ;
   	 12'd27  	 : out = 12'd2411  	 ;
   	 12'd28  	 : out = 12'd2500  	 ;
   	 12'd29  	 : out = 12'd2589  	 ;
   	 12'd30  	 : out = 12'd2679  	 ;
   	 12'd31  	 : out = 12'd2768  	 ;
   	 12'd32  	 : out = 12'd2857  	 ;
   	 12'd33  	 : out = 12'd2946  	 ;
   	 12'd34  	 : out = 12'd3036  	 ;
   	 12'd35  	 : out = 12'd3125  	 ;
   	 12'd36  	 : out = 12'd3214  	 ;
   	 12'd37  	 : out = 12'd3304  	 ;
   	 12'd38  	 : out = 12'd3393  	 ;
   	 12'd39  	 : out = 12'd3482  	 ;
   	 12'd40  	 : out = 12'd3572  	 ;
   	 12'd41  	 : out = 12'd3661  	 ;
   	 12'd42  	 : out = 12'd3750  	 ;
   	 12'd43  	 : out = 12'd3839  	 ;
   	 12'd44  	 : out = 12'd3929  	 ;
   	 12'd45  	 : out = 12'd4018  	 ;


		default: begin
					out = 12'bZZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule
