module F_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
		 12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd29  	 ;
   	 12'd2  	 : out = 12'd59  	 ;
   	 12'd3  	 : out = 12'd89  	 ;
   	 12'd4  	 : out = 12'd119  	 ;
   	 12'd5  	 : out = 12'd149  	 ;
   	 12'd6  	 : out = 12'd178  	 ;
   	 12'd7  	 : out = 12'd208  	 ;
   	 12'd8  	 : out = 12'd238  	 ;
   	 12'd9  	 : out = 12'd268  	 ;
   	 12'd10  	 : out = 12'd298  	 ;
   	 12'd11  	 : out = 12'd327  	 ;
   	 12'd12  	 : out = 12'd357  	 ;
   	 12'd13  	 : out = 12'd387  	 ;
   	 12'd14  	 : out = 12'd417  	 ;
   	 12'd15  	 : out = 12'd447  	 ;
   	 12'd16  	 : out = 12'd476  	 ;
   	 12'd17  	 : out = 12'd506  	 ;
   	 12'd18  	 : out = 12'd536  	 ;
   	 12'd19  	 : out = 12'd566  	 ;
   	 12'd20  	 : out = 12'd596  	 ;
   	 12'd21  	 : out = 12'd625  	 ;
   	 12'd22  	 : out = 12'd655  	 ;
   	 12'd23  	 : out = 12'd685  	 ;
   	 12'd24  	 : out = 12'd715  	 ;
   	 12'd25  	 : out = 12'd745  	 ;
   	 12'd26  	 : out = 12'd774  	 ;
   	 12'd27  	 : out = 12'd804  	 ;
   	 12'd28  	 : out = 12'd834  	 ;
   	 12'd29  	 : out = 12'd864  	 ;
   	 12'd30  	 : out = 12'd894  	 ;
   	 12'd31  	 : out = 12'd923  	 ;
   	 12'd32  	 : out = 12'd953  	 ;
   	 12'd33  	 : out = 12'd983  	 ;
   	 12'd34  	 : out = 12'd1013  	 ;
   	 12'd35  	 : out = 12'd1043  	 ;
   	 12'd36  	 : out = 12'd1072  	 ;
   	 12'd37  	 : out = 12'd1102  	 ;
   	 12'd38  	 : out = 12'd1132  	 ;
   	 12'd39  	 : out = 12'd1162  	 ;
   	 12'd40  	 : out = 12'd1192  	 ;
   	 12'd41  	 : out = 12'd1221  	 ;
   	 12'd42  	 : out = 12'd1251  	 ;
   	 12'd43  	 : out = 12'd1281  	 ;
   	 12'd44  	 : out = 12'd1311  	 ;
   	 12'd45  	 : out = 12'd1341  	 ;
   	 12'd46  	 : out = 12'd1370  	 ;
   	 12'd47  	 : out = 12'd1400  	 ;
   	 12'd48  	 : out = 12'd1430  	 ;
   	 12'd49  	 : out = 12'd1460  	 ;
   	 12'd50  	 : out = 12'd1490  	 ;
   	 12'd51  	 : out = 12'd1519  	 ;
   	 12'd52  	 : out = 12'd1549  	 ;
   	 12'd53  	 : out = 12'd1579  	 ;
   	 12'd54  	 : out = 12'd1609  	 ;
   	 12'd55  	 : out = 12'd1639  	 ;
   	 12'd56  	 : out = 12'd1668  	 ;
   	 12'd57  	 : out = 12'd1698  	 ;
   	 12'd58  	 : out = 12'd1728  	 ;
   	 12'd59  	 : out = 12'd1758  	 ;
   	 12'd60  	 : out = 12'd1788  	 ;
   	 12'd61  	 : out = 12'd1817  	 ;
   	 12'd62  	 : out = 12'd1847  	 ;
   	 12'd63  	 : out = 12'd1877  	 ;
   	 12'd64  	 : out = 12'd1907  	 ;
   	 12'd65  	 : out = 12'd1937  	 ;
   	 12'd66  	 : out = 12'd1966  	 ;
   	 12'd67  	 : out = 12'd1996  	 ;
   	 12'd68  	 : out = 12'd2026  	 ;
   	 12'd69  	 : out = 12'd2056  	 ;
   	 12'd70  	 : out = 12'd2086  	 ;
   	 12'd71  	 : out = 12'd2115  	 ;
   	 12'd72  	 : out = 12'd2145  	 ;
   	 12'd73  	 : out = 12'd2175  	 ;
   	 12'd74  	 : out = 12'd2205  	 ;
   	 12'd75  	 : out = 12'd2235  	 ;
   	 12'd76  	 : out = 12'd2264  	 ;
   	 12'd77  	 : out = 12'd2294  	 ;
   	 12'd78  	 : out = 12'd2324  	 ;
   	 12'd79  	 : out = 12'd2354  	 ;
   	 12'd80  	 : out = 12'd2384  	 ;
   	 12'd81  	 : out = 12'd2413  	 ;
   	 12'd82  	 : out = 12'd2443  	 ;
   	 12'd83  	 : out = 12'd2473  	 ;
   	 12'd84  	 : out = 12'd2503  	 ;
   	 12'd85  	 : out = 12'd2533  	 ;
   	 12'd86  	 : out = 12'd2562  	 ;
   	 12'd87  	 : out = 12'd2592  	 ;
   	 12'd88  	 : out = 12'd2622  	 ;
   	 12'd89  	 : out = 12'd2652  	 ;
   	 12'd90  	 : out = 12'd2682  	 ;
   	 12'd91  	 : out = 12'd2711  	 ;
   	 12'd92  	 : out = 12'd2741  	 ;
   	 12'd93  	 : out = 12'd2771  	 ;
   	 12'd94  	 : out = 12'd2801  	 ;
   	 12'd95  	 : out = 12'd2831  	 ;
   	 12'd96  	 : out = 12'd2860  	 ;
   	 12'd97  	 : out = 12'd2890  	 ;
   	 12'd98  	 : out = 12'd2920  	 ;
   	 12'd99  	 : out = 12'd2950  	 ;
   	 12'd100  	 : out = 12'd2980  	 ;
   	 12'd101  	 : out = 12'd3009  	 ;
   	 12'd102  	 : out = 12'd3039  	 ;
   	 12'd103  	 : out = 12'd3069  	 ;
   	 12'd104  	 : out = 12'd3099  	 ;
   	 12'd105  	 : out = 12'd3129  	 ;
   	 12'd106  	 : out = 12'd3158  	 ;
   	 12'd107  	 : out = 12'd3188  	 ;
   	 12'd108  	 : out = 12'd3218  	 ;
   	 12'd109  	 : out = 12'd3248  	 ;
   	 12'd110  	 : out = 12'd3278  	 ;
   	 12'd111  	 : out = 12'd3307  	 ;
   	 12'd112  	 : out = 12'd3337  	 ;
   	 12'd113  	 : out = 12'd3367  	 ;
   	 12'd114  	 : out = 12'd3397  	 ;
   	 12'd115  	 : out = 12'd3427  	 ;
   	 12'd116  	 : out = 12'd3456  	 ;
   	 12'd117  	 : out = 12'd3486  	 ;
   	 12'd118  	 : out = 12'd3516  	 ;
   	 12'd119  	 : out = 12'd3546  	 ;
   	 12'd120  	 : out = 12'd3576  	 ;
   	 12'd121  	 : out = 12'd3605  	 ;
   	 12'd122  	 : out = 12'd3635  	 ;
   	 12'd123  	 : out = 12'd3665  	 ;
   	 12'd124  	 : out = 12'd3695  	 ;
   	 12'd125  	 : out = 12'd3725  	 ;
   	 12'd126  	 : out = 12'd3754  	 ;
   	 12'd127  	 : out = 12'd3784  	 ;
   	 12'd128  	 : out = 12'd3814  	 ;
   	 12'd129  	 : out = 12'd3844  	 ;
   	 12'd130  	 : out = 12'd3874  	 ;
   	 12'd131  	 : out = 12'd3903  	 ;
   	 12'd132  	 : out = 12'd3933  	 ;
   	 12'd133  	 : out = 12'd3963  	 ;
   	 12'd134  	 : out = 12'd3993  	 ;
   	 12'd135  	 : out = 12'd4023  	 ;
   	 12'd136  	 : out = 12'd4052  	 ;
   	 12'd137  	 : out = 12'd4082  	 ;

		default: begin
					out = 12'bZZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule

module F_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)

	12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd59  	 ;
   	 12'd2  	 : out = 12'd119  	 ;
   	 12'd3  	 : out = 12'd178  	 ;
   	 12'd4  	 : out = 12'd238  	 ;
   	 12'd5  	 : out = 12'd298  	 ;
   	 12'd6  	 : out = 12'd357  	 ;
   	 12'd7  	 : out = 12'd417  	 ;
   	 12'd8  	 : out = 12'd476  	 ;
   	 12'd9  	 : out = 12'd536  	 ;
   	 12'd10  	 : out = 12'd596  	 ;
   	 12'd11  	 : out = 12'd655  	 ;
   	 12'd12  	 : out = 12'd715  	 ;
   	 12'd13  	 : out = 12'd774  	 ;
   	 12'd14  	 : out = 12'd834  	 ;
   	 12'd15  	 : out = 12'd894  	 ;
   	 12'd16  	 : out = 12'd953  	 ;
   	 12'd17  	 : out = 12'd1013  	 ;
   	 12'd18  	 : out = 12'd1072  	 ;
   	 12'd19  	 : out = 12'd1132  	 ;
   	 12'd20  	 : out = 12'd1192  	 ;
   	 12'd21  	 : out = 12'd1251  	 ;
   	 12'd22  	 : out = 12'd1311  	 ;
   	 12'd23  	 : out = 12'd1370  	 ;
   	 12'd24  	 : out = 12'd1430  	 ;
   	 12'd25  	 : out = 12'd1490  	 ;
   	 12'd26  	 : out = 12'd1549  	 ;
   	 12'd27  	 : out = 12'd1609  	 ;
   	 12'd28  	 : out = 12'd1668  	 ;
   	 12'd29  	 : out = 12'd1728  	 ;
   	 12'd30  	 : out = 12'd1788  	 ;
   	 12'd31  	 : out = 12'd1847  	 ;
   	 12'd32  	 : out = 12'd1907  	 ;
   	 12'd33  	 : out = 12'd1966  	 ;
   	 12'd34  	 : out = 12'd2026  	 ;
   	 12'd35  	 : out = 12'd2086  	 ;
   	 12'd36  	 : out = 12'd2145  	 ;
   	 12'd37  	 : out = 12'd2205  	 ;
   	 12'd38  	 : out = 12'd2264  	 ;
   	 12'd39  	 : out = 12'd2324  	 ;
   	 12'd40  	 : out = 12'd2384  	 ;
   	 12'd41  	 : out = 12'd2443  	 ;
   	 12'd42  	 : out = 12'd2503  	 ;
   	 12'd43  	 : out = 12'd2562  	 ;
   	 12'd44  	 : out = 12'd2622  	 ;
   	 12'd45  	 : out = 12'd2682  	 ;
   	 12'd46  	 : out = 12'd2741  	 ;
   	 12'd47  	 : out = 12'd2801  	 ;
   	 12'd48  	 : out = 12'd2860  	 ;
   	 12'd49  	 : out = 12'd2920  	 ;
   	 12'd50  	 : out = 12'd2980  	 ;
   	 12'd51  	 : out = 12'd3039  	 ;
   	 12'd52  	 : out = 12'd3099  	 ;
   	 12'd53  	 : out = 12'd3158  	 ;
   	 12'd54  	 : out = 12'd3218  	 ;
   	 12'd55  	 : out = 12'd3278  	 ;
   	 12'd56  	 : out = 12'd3337  	 ;
   	 12'd57  	 : out = 12'd3397  	 ;
   	 12'd58  	 : out = 12'd3456  	 ;
   	 12'd59  	 : out = 12'd3516  	 ;
   	 12'd60  	 : out = 12'd3576  	 ;
   	 12'd61  	 : out = 12'd3635  	 ;
   	 12'd62  	 : out = 12'd3695  	 ;
   	 12'd63  	 : out = 12'd3754  	 ;
   	 12'd64  	 : out = 12'd3814  	 ;
   	 12'd65  	 : out = 12'd3874  	 ;
   	 12'd66  	 : out = 12'd3933  	 ;
   	 12'd67  	 : out = 12'd3993  	 ;
   	 12'd68  	 : out = 12'd4052	 	 ;

		default: begin
					out = 12'bZZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule
