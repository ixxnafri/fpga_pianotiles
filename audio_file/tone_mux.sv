module tone_checker	(input logic[15:0] c_4,
							 input logic[15:0] d_4,
							 input logic[15:0] e_4,
							 input logic[15:0] f_4,
							 input logic[15:0] g_4,
							 input logic[15:0] a_4,
							 input logic[15:0] b_4,
							 input logic[15:0] c_5,
							 input logic[15:0] d_5,
							 input logic[15:0] e_5,
							 input logic[15:0] f_5,
							 input logic[15:0] g_5,
							 input logic[15:0] a_5,
							 input logic[15:0] b_5,
							 input logic[15:0] c_6,
							 input logic[3:0] select,
							 output logic[15:0] out);
							 
always_comb begin
    unique case (select)
			4'd0: out = c_4;
			4'd1: out = d_4;
			4'd2: out = e_4;
			4'd3: out = f_4;
			4'd4: out = g_4;
			4'd5: out = a_4;
			4'd6: out = b_4;
			4'd7: out = c_5;
			4'd8: out = d_5;
			4'd9: out = e_5;
			4'd10: out = f_5;
			4'd11: out = g_5;
			4'd12: out = a_5;
			4'd13: out = b_5;
			4'd14: out = c_6;
			4'd15: out = 16'd0;
			
	
    endcase
	 end
endmodule

 