module addition_synthesis(input logic[15:0] in1,
								  input logic[15:0] in2,
								  output logic[15:0] out);

logic[31:0] sum;
logic [31:0] temp_out;
assign out = temp_out[15:0];
assign sum = {{16{in1[15]}}, in1} + {{16{in2[15]}}, in2};

		always_comb begin	
				if(in1 == 32'd0 || in2 == 32'd0)
					temp_out = sum;
				else
					temp_out = sum/32'd2;
		end
		
endmodule