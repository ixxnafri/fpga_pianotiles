module G_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
   	 12'd1  	 : out = 12'd33  	 ;
   	 12'd2  	 : out = 12'd66  	 ;
   	 12'd3  	 : out = 12'd100  	 ;
   	 12'd4  	 : out = 12'd133  	 ;
   	 12'd5  	 : out = 12'd167  	 ;
   	 12'd6  	 : out = 12'd200  	 ;
   	 12'd7  	 : out = 12'd234  	 ;
   	 12'd8  	 : out = 12'd267  	 ;
   	 12'd9  	 : out = 12'd301  	 ;
   	 12'd10  	 : out = 12'd334  	 ;
   	 12'd11  	 : out = 12'd367  	 ;
   	 12'd12  	 : out = 12'd401  	 ;
   	 12'd13  	 : out = 12'd434  	 ;
   	 12'd14  	 : out = 12'd468  	 ;
   	 12'd15  	 : out = 12'd501  	 ;
   	 12'd16  	 : out = 12'd535  	 ;
   	 12'd17  	 : out = 12'd568  	 ;
   	 12'd18  	 : out = 12'd602  	 ;
   	 12'd19  	 : out = 12'd635  	 ;
   	 12'd20  	 : out = 12'd669  	 ;
   	 12'd21  	 : out = 12'd702  	 ;
   	 12'd22  	 : out = 12'd735  	 ;
   	 12'd23  	 : out = 12'd769  	 ;
   	 12'd24  	 : out = 12'd802  	 ;
   	 12'd25  	 : out = 12'd836  	 ;
   	 12'd26  	 : out = 12'd869  	 ;
   	 12'd27  	 : out = 12'd903  	 ;
   	 12'd28  	 : out = 12'd936  	 ;
   	 12'd29  	 : out = 12'd970  	 ;
   	 12'd30  	 : out = 12'd1003  	 ;
   	 12'd31  	 : out = 12'd1036  	 ;
   	 12'd32  	 : out = 12'd1070  	 ;
   	 12'd33  	 : out = 12'd1103  	 ;
   	 12'd34  	 : out = 12'd1137  	 ;
   	 12'd35  	 : out = 12'd1170  	 ;
   	 12'd36  	 : out = 12'd1204  	 ;
   	 12'd37  	 : out = 12'd1237  	 ;
   	 12'd38  	 : out = 12'd1271  	 ;
   	 12'd39  	 : out = 12'd1304  	 ;
   	 12'd40  	 : out = 12'd1338  	 ;
   	 12'd41  	 : out = 12'd1371  	 ;
   	 12'd42  	 : out = 12'd1404  	 ;
   	 12'd43  	 : out = 12'd1438  	 ;
   	 12'd44  	 : out = 12'd1471  	 ;
   	 12'd45  	 : out = 12'd1505  	 ;
   	 12'd46  	 : out = 12'd1538  	 ;
   	 12'd47  	 : out = 12'd1572  	 ;
   	 12'd48  	 : out = 12'd1605  	 ;
   	 12'd49  	 : out = 12'd1639  	 ;
   	 12'd50  	 : out = 12'd1672  	 ;
   	 12'd51  	 : out = 12'd1705  	 ;
   	 12'd52  	 : out = 12'd1739  	 ;
   	 12'd53  	 : out = 12'd1772  	 ;
   	 12'd54  	 : out = 12'd1806  	 ;
   	 12'd55  	 : out = 12'd1839  	 ;
   	 12'd56  	 : out = 12'd1873  	 ;
   	 12'd57  	 : out = 12'd1906  	 ;
   	 12'd58  	 : out = 12'd1940  	 ;
   	 12'd59  	 : out = 12'd1973  	 ;
   	 12'd60  	 : out = 12'd2007  	 ;
   	 12'd61  	 : out = 12'd2040  	 ;
   	 12'd62  	 : out = 12'd2073  	 ;
   	 12'd63  	 : out = 12'd2107  	 ;
   	 12'd64  	 : out = 12'd2140  	 ;
   	 12'd65  	 : out = 12'd2174  	 ;
   	 12'd66  	 : out = 12'd2207  	 ;
   	 12'd67  	 : out = 12'd2241  	 ;
   	 12'd68  	 : out = 12'd2274  	 ;
   	 12'd69  	 : out = 12'd2308  	 ;
   	 12'd70  	 : out = 12'd2341  	 ;
   	 12'd71  	 : out = 12'd2374  	 ;
   	 12'd72  	 : out = 12'd2408  	 ;
   	 12'd73  	 : out = 12'd2441  	 ;
   	 12'd74  	 : out = 12'd2475  	 ;
   	 12'd75  	 : out = 12'd2508  	 ;
   	 12'd76  	 : out = 12'd2542  	 ;
   	 12'd77  	 : out = 12'd2575  	 ;
   	 12'd78  	 : out = 12'd2609  	 ;
   	 12'd79  	 : out = 12'd2642  	 ;
   	 12'd80  	 : out = 12'd2676  	 ;
   	 12'd81  	 : out = 12'd2709  	 ;
   	 12'd82  	 : out = 12'd2742  	 ;
   	 12'd83  	 : out = 12'd2776  	 ;
   	 12'd84  	 : out = 12'd2809  	 ;
   	 12'd85  	 : out = 12'd2843  	 ;
   	 12'd86  	 : out = 12'd2876  	 ;
   	 12'd87  	 : out = 12'd2910  	 ;
   	 12'd88  	 : out = 12'd2943  	 ;
   	 12'd89  	 : out = 12'd2977  	 ;
   	 12'd90  	 : out = 12'd3010  	 ;
   	 12'd91  	 : out = 12'd3044  	 ;
   	 12'd92  	 : out = 12'd3077  	 ;
   	 12'd93  	 : out = 12'd3110  	 ;
   	 12'd94  	 : out = 12'd3144  	 ;
   	 12'd95  	 : out = 12'd3177  	 ;
   	 12'd96  	 : out = 12'd3211  	 ;
   	 12'd97  	 : out = 12'd3244  	 ;
   	 12'd98  	 : out = 12'd3278  	 ;
   	 12'd99  	 : out = 12'd3311  	 ;
   	 12'd100  	 : out = 12'd3345  	 ;
   	 12'd101  	 : out = 12'd3378  	 ;
   	 12'd102  	 : out = 12'd3411  	 ;
   	 12'd103  	 : out = 12'd3445  	 ;
   	 12'd104  	 : out = 12'd3478  	 ;
   	 12'd105  	 : out = 12'd3512  	 ;
   	 12'd106  	 : out = 12'd3545  	 ;
   	 12'd107  	 : out = 12'd3579  	 ;
   	 12'd108  	 : out = 12'd3612  	 ;
   	 12'd109  	 : out = 12'd3646  	 ;
   	 12'd110  	 : out = 12'd3679  	 ;
   	 12'd111  	 : out = 12'd3713  	 ;
   	 12'd112  	 : out = 12'd3746  	 ;
   	 12'd113  	 : out = 12'd3779  	 ;
   	 12'd114  	 : out = 12'd3813  	 ;
   	 12'd115  	 : out = 12'd3846  	 ;
   	 12'd116  	 : out = 12'd3880  	 ;
   	 12'd117  	 : out = 12'd3913  	 ;
   	 12'd118  	 : out = 12'd3947  	 ;
   	 12'd119  	 : out = 12'd3980  	 ;
   	 12'd120  	 : out = 12'd4014  	 ;
   	 12'd121  	 : out = 12'd4047  	 ;
   	 12'd122  	 : out = 12'd4080  	 ;
		default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule

module G_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
	12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd66  	 ;
   	 12'd2  	 : out = 12'd133  	 ;
   	 12'd3  	 : out = 12'd200  	 ;
   	 12'd4  	 : out = 12'd267  	 ;
   	 12'd5  	 : out = 12'd334  	 ;
   	 12'd6  	 : out = 12'd401  	 ;
   	 12'd7  	 : out = 12'd468  	 ;
   	 12'd8  	 : out = 12'd535  	 ;
   	 12'd9  	 : out = 12'd602  	 ;
   	 12'd10  	 : out = 12'd669  	 ;
   	 12'd11  	 : out = 12'd735  	 ;
   	 12'd12  	 : out = 12'd802  	 ;
   	 12'd13  	 : out = 12'd869  	 ;
   	 12'd14  	 : out = 12'd936  	 ;
   	 12'd15  	 : out = 12'd1003  	 ;
   	 12'd16  	 : out = 12'd1070  	 ;
   	 12'd17  	 : out = 12'd1137  	 ;
   	 12'd18  	 : out = 12'd1204  	 ;
   	 12'd19  	 : out = 12'd1271  	 ;
   	 12'd20  	 : out = 12'd1338  	 ;
   	 12'd21  	 : out = 12'd1404  	 ;
   	 12'd22  	 : out = 12'd1471  	 ;
   	 12'd23  	 : out = 12'd1538  	 ;
   	 12'd24  	 : out = 12'd1605  	 ;
   	 12'd25  	 : out = 12'd1672  	 ;
   	 12'd26  	 : out = 12'd1739  	 ;
   	 12'd27  	 : out = 12'd1806  	 ;
   	 12'd28  	 : out = 12'd1873  	 ;
   	 12'd29  	 : out = 12'd1940  	 ;
   	 12'd30  	 : out = 12'd2007  	 ;
   	 12'd31  	 : out = 12'd2073  	 ;
   	 12'd32  	 : out = 12'd2140  	 ;
   	 12'd33  	 : out = 12'd2207  	 ;
   	 12'd34  	 : out = 12'd2274  	 ;
   	 12'd35  	 : out = 12'd2341  	 ;
   	 12'd36  	 : out = 12'd2408  	 ;
   	 12'd37  	 : out = 12'd2475  	 ;
   	 12'd38  	 : out = 12'd2542  	 ;
   	 12'd39  	 : out = 12'd2609  	 ;
   	 12'd40  	 : out = 12'd2676  	 ;
   	 12'd41  	 : out = 12'd2742  	 ;
   	 12'd42  	 : out = 12'd2809  	 ;
   	 12'd43  	 : out = 12'd2876  	 ;
   	 12'd44  	 : out = 12'd2943  	 ;
   	 12'd45  	 : out = 12'd3010  	 ;
   	 12'd46  	 : out = 12'd3077  	 ;
   	 12'd47  	 : out = 12'd3144  	 ;
   	 12'd48  	 : out = 12'd3211  	 ;
   	 12'd49  	 : out = 12'd3278  	 ;
   	 12'd50  	 : out = 12'd3345  	 ;
   	 12'd51  	 : out = 12'd3411  	 ;
   	 12'd52  	 : out = 12'd3478  	 ;
   	 12'd53  	 : out = 12'd3545  	 ;
   	 12'd54  	 : out = 12'd3612  	 ;
   	 12'd55  	 : out = 12'd3679  	 ;
   	 12'd56  	 : out = 12'd3746  	 ;
   	 12'd57  	 : out = 12'd3813  	 ;
   	 12'd58  	 : out = 12'd3880  	 ;
   	 12'd59  	 : out = 12'd3947  	 ;
   	 12'd60  	 : out = 12'd4014  	 ;
   	 12'd61  	 : out = 12'd4080  	 ;

		default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule
