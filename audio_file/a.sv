module A_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
	12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd37  	 ;
   	 12'd2  	 : out = 12'd75  	 ;
   	 12'd3  	 : out = 12'd112  	 ;
   	 12'd4  	 : out = 12'd150  	 ;
   	 12'd5  	 : out = 12'd187  	 ;
   	 12'd6  	 : out = 12'd225  	 ;
   	 12'd7  	 : out = 12'd262  	 ;
   	 12'd8  	 : out = 12'd300  	 ;
   	 12'd9  	 : out = 12'd337  	 ;
   	 12'd10  	 : out = 12'd375  	 ;
   	 12'd11  	 : out = 12'd413  	 ;
   	 12'd12  	 : out = 12'd450  	 ;
   	 12'd13  	 : out = 12'd488  	 ;
   	 12'd14  	 : out = 12'd525  	 ;
   	 12'd15  	 : out = 12'd563  	 ;
   	 12'd16  	 : out = 12'd600  	 ;
   	 12'd17  	 : out = 12'd638  	 ;
   	 12'd18  	 : out = 12'd675  	 ;
   	 12'd19  	 : out = 12'd713  	 ;
   	 12'd20  	 : out = 12'd750  	 ;
   	 12'd21  	 : out = 12'd788  	 ;
   	 12'd22  	 : out = 12'd826  	 ;
   	 12'd23  	 : out = 12'd863  	 ;
   	 12'd24  	 : out = 12'd901  	 ;
   	 12'd25  	 : out = 12'd938  	 ;
   	 12'd26  	 : out = 12'd976  	 ;
   	 12'd27  	 : out = 12'd1013  	 ;
   	 12'd28  	 : out = 12'd1051  	 ;
   	 12'd29  	 : out = 12'd1088  	 ;
   	 12'd30  	 : out = 12'd1126  	 ;
   	 12'd31  	 : out = 12'd1163  	 ;
   	 12'd32  	 : out = 12'd1201  	 ;
   	 12'd33  	 : out = 12'd1239  	 ;
   	 12'd34  	 : out = 12'd1276  	 ;
   	 12'd35  	 : out = 12'd1314  	 ;
   	 12'd36  	 : out = 12'd1351  	 ;
   	 12'd37  	 : out = 12'd1389  	 ;
   	 12'd38  	 : out = 12'd1426  	 ;
   	 12'd39  	 : out = 12'd1464  	 ;
   	 12'd40  	 : out = 12'd1501  	 ;
   	 12'd41  	 : out = 12'd1539  	 ;
   	 12'd42  	 : out = 12'd1576  	 ;
   	 12'd43  	 : out = 12'd1614  	 ;
   	 12'd44  	 : out = 12'd1652  	 ;
   	 12'd45  	 : out = 12'd1689  	 ;
   	 12'd46  	 : out = 12'd1727  	 ;
   	 12'd47  	 : out = 12'd1764  	 ;
   	 12'd48  	 : out = 12'd1802  	 ;
   	 12'd49  	 : out = 12'd1839  	 ;
   	 12'd50  	 : out = 12'd1877  	 ;
   	 12'd51  	 : out = 12'd1914  	 ;
   	 12'd52  	 : out = 12'd1952  	 ;
   	 12'd53  	 : out = 12'd1989  	 ;
   	 12'd54  	 : out = 12'd2027  	 ;
   	 12'd55  	 : out = 12'd2065  	 ;
   	 12'd56  	 : out = 12'd2102  	 ;
   	 12'd57  	 : out = 12'd2140  	 ;
   	 12'd58  	 : out = 12'd2177  	 ;
   	 12'd59  	 : out = 12'd2215  	 ;
   	 12'd60  	 : out = 12'd2252  	 ;
   	 12'd61  	 : out = 12'd2290  	 ;
   	 12'd62  	 : out = 12'd2327  	 ;
   	 12'd63  	 : out = 12'd2365  	 ;
   	 12'd64  	 : out = 12'd2402  	 ;
   	 12'd65  	 : out = 12'd2440  	 ;
   	 12'd66  	 : out = 12'd2478  	 ;
   	 12'd67  	 : out = 12'd2515  	 ;
   	 12'd68  	 : out = 12'd2553  	 ;
   	 12'd69  	 : out = 12'd2590  	 ;
   	 12'd70  	 : out = 12'd2628  	 ;
   	 12'd71  	 : out = 12'd2665  	 ;
   	 12'd72  	 : out = 12'd2703  	 ;
   	 12'd73  	 : out = 12'd2740  	 ;
   	 12'd74  	 : out = 12'd2778  	 ;
   	 12'd75  	 : out = 12'd2815  	 ;
   	 12'd76  	 : out = 12'd2853  	 ;
   	 12'd77  	 : out = 12'd2891  	 ;
   	 12'd78  	 : out = 12'd2928  	 ;
   	 12'd79  	 : out = 12'd2966  	 ;
   	 12'd80  	 : out = 12'd3003  	 ;
   	 12'd81  	 : out = 12'd3041  	 ;
   	 12'd82  	 : out = 12'd3078  	 ;
   	 12'd83  	 : out = 12'd3116  	 ;
   	 12'd84  	 : out = 12'd3153  	 ;
   	 12'd85  	 : out = 12'd3191  	 ;
   	 12'd86  	 : out = 12'd3229  	 ;
   	 12'd87  	 : out = 12'd3266  	 ;
   	 12'd88  	 : out = 12'd3304  	 ;
   	 12'd89  	 : out = 12'd3341  	 ;
   	 12'd90  	 : out = 12'd3379  	 ;
   	 12'd91  	 : out = 12'd3416  	 ;
   	 12'd92  	 : out = 12'd3454  	 ;
   	 12'd93  	 : out = 12'd3491  	 ;
   	 12'd94  	 : out = 12'd3529  	 ;
   	 12'd95  	 : out = 12'd3566  	 ;
   	 12'd96  	 : out = 12'd3604  	 ;
   	 12'd97  	 : out = 12'd3642  	 ;
   	 12'd98  	 : out = 12'd3679  	 ;
   	 12'd99  	 : out = 12'd3717  	 ;
   	 12'd100  	 : out = 12'd3754  	 ;
   	 12'd101  	 : out = 12'd3792  	 ;
   	 12'd102  	 : out = 12'd3829  	 ;
   	 12'd103  	 : out = 12'd3867  	 ;
   	 12'd104  	 : out = 12'd3904  	 ;
   	 12'd105  	 : out = 12'd3942  	 ;
   	 12'd106  	 : out = 12'd3979  	 ;
   	 12'd107  	 : out = 12'd4017  	 ;
   	 12'd108  	 : out = 12'd4055  	 ;
   	 12'd109  	 : out = 12'd4092  	 ;

							default: begin
								out = 12'bZZZZZZZZZZZ;
							end
	
    endcase
	 end
endmodule

module A_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd75  	 ;
   	 12'd2  	 : out = 12'd150  	 ;
   	 12'd3  	 : out = 12'd225  	 ;
   	 12'd4  	 : out = 12'd300  	 ;
   	 12'd5  	 : out = 12'd375  	 ;
   	 12'd6  	 : out = 12'd450  	 ;
   	 12'd7  	 : out = 12'd525  	 ;
   	 12'd8  	 : out = 12'd600  	 ;
   	 12'd9  	 : out = 12'd675  	 ;
   	 12'd10  	 : out = 12'd750  	 ;
   	 12'd11  	 : out = 12'd826  	 ;
   	 12'd12  	 : out = 12'd901  	 ;
   	 12'd13  	 : out = 12'd976  	 ;
   	 12'd14  	 : out = 12'd1051  	 ;
   	 12'd15  	 : out = 12'd1126  	 ;
   	 12'd16  	 : out = 12'd1201  	 ;
   	 12'd17  	 : out = 12'd1276  	 ;
   	 12'd18  	 : out = 12'd1351  	 ;
   	 12'd19  	 : out = 12'd1426  	 ;
   	 12'd20  	 : out = 12'd1501  	 ;
   	 12'd21  	 : out = 12'd1576  	 ;
   	 12'd22  	 : out = 12'd1652  	 ;
   	 12'd23  	 : out = 12'd1727  	 ;
   	 12'd24  	 : out = 12'd1802  	 ;
   	 12'd25  	 : out = 12'd1877  	 ;
   	 12'd26  	 : out = 12'd1952  	 ;
   	 12'd27  	 : out = 12'd2027  	 ;
   	 12'd28  	 : out = 12'd2102  	 ;
   	 12'd29  	 : out = 12'd2177  	 ;
   	 12'd30  	 : out = 12'd2252  	 ;
   	 12'd31  	 : out = 12'd2327  	 ;
   	 12'd32  	 : out = 12'd2402  	 ;
   	 12'd33  	 : out = 12'd2478  	 ;
   	 12'd34  	 : out = 12'd2553  	 ;
   	 12'd35  	 : out = 12'd2628  	 ;
   	 12'd36  	 : out = 12'd2703  	 ;
   	 12'd37  	 : out = 12'd2778  	 ;
   	 12'd38  	 : out = 12'd2853  	 ;
   	 12'd39  	 : out = 12'd2928  	 ;
   	 12'd40  	 : out = 12'd3003  	 ;
   	 12'd41  	 : out = 12'd3078  	 ;
   	 12'd42  	 : out = 12'd3153  	 ;
   	 12'd43  	 : out = 12'd3229  	 ;
   	 12'd44  	 : out = 12'd3304  	 ;
   	 12'd45  	 : out = 12'd3379  	 ;
   	 12'd46  	 : out = 12'd3454  	 ;
   	 12'd47  	 : out = 12'd3529  	 ;
   	 12'd48  	 : out = 12'd3604  	 ;
   	 12'd49  	 : out = 12'd3679  	 ;
   	 12'd50  	 : out = 12'd3754  	 ;
   	 12'd51  	 : out = 12'd3829  	 ;
   	 12'd52  	 : out = 12'd3904  	 ;
   	 12'd53  	 : out = 12'd3979  	 ;
   	 12'd54  	 : out = 12'd4055  	 ;


							default: begin
								out = 12'bZZZZZZZZZZZ;
							end
	
    endcase
	 end
endmodule

