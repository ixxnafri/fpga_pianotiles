module fp_multiplier (input logic [15:0] sample,
							 input logic [31:0] fixed_point,
							 output logic [15:0] out);

logic[31:0] extended;
logic[63:0] multiplied;
logic[63:0] mid;
logic[15:0] sum, samp;
assign extended = {sample[15:0],{16{1'b0}}};
				always_comb begin
					multiplied = {{32{sample[15]}},extended}*{{32{1'b0}},fixed_point};
					mid = multiplied + (64'hF0000000 & multiplied[31]);
					out = mid[47:32];
				end
endmodule 