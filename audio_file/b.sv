module B_4( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
    	12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd42  	 ;
   	 12'd2  	 : out = 12'd84  	 ;
   	 12'd3  	 : out = 12'd126  	 ;
   	 12'd4  	 : out = 12'd168  	 ;
   	 12'd5  	 : out = 12'd210  	 ;
   	 12'd6  	 : out = 12'd252  	 ;
   	 12'd7  	 : out = 12'd295  	 ;
   	 12'd8  	 : out = 12'd337  	 ;
   	 12'd9  	 : out = 12'd379  	 ;
   	 12'd10  	 : out = 12'd421  	 ;
   	 12'd11  	 : out = 12'd463  	 ;
   	 12'd12  	 : out = 12'd505  	 ;
   	 12'd13  	 : out = 12'd547  	 ;
   	 12'd14  	 : out = 12'd590  	 ;
   	 12'd15  	 : out = 12'd632  	 ;
   	 12'd16  	 : out = 12'd674  	 ;
   	 12'd17  	 : out = 12'd716  	 ;
   	 12'd18  	 : out = 12'd758  	 ;
   	 12'd19  	 : out = 12'd800  	 ;
   	 12'd20  	 : out = 12'd842  	 ;
   	 12'd21  	 : out = 12'd885  	 ;
   	 12'd22  	 : out = 12'd927  	 ;
   	 12'd23  	 : out = 12'd969  	 ;
   	 12'd24  	 : out = 12'd1011  	 ;
   	 12'd25  	 : out = 12'd1053  	 ;
   	 12'd26  	 : out = 12'd1095  	 ;
   	 12'd27  	 : out = 12'd1137  	 ;
   	 12'd28  	 : out = 12'd1180  	 ;
   	 12'd29  	 : out = 12'd1222  	 ;
   	 12'd30  	 : out = 12'd1264  	 ;
   	 12'd31  	 : out = 12'd1306  	 ;
   	 12'd32  	 : out = 12'd1348  	 ;
   	 12'd33  	 : out = 12'd1390  	 ;
   	 12'd34  	 : out = 12'd1432  	 ;
   	 12'd35  	 : out = 12'd1475  	 ;
   	 12'd36  	 : out = 12'd1517  	 ;
   	 12'd37  	 : out = 12'd1559  	 ;
   	 12'd38  	 : out = 12'd1601  	 ;
   	 12'd39  	 : out = 12'd1643  	 ;
   	 12'd40  	 : out = 12'd1685  	 ;
   	 12'd41  	 : out = 12'd1727  	 ;
   	 12'd42  	 : out = 12'd1770  	 ;
   	 12'd43  	 : out = 12'd1812  	 ;
   	 12'd44  	 : out = 12'd1854  	 ;
   	 12'd45  	 : out = 12'd1896  	 ;
   	 12'd46  	 : out = 12'd1938  	 ;
   	 12'd47  	 : out = 12'd1980  	 ;
   	 12'd48  	 : out = 12'd2022  	 ;
   	 12'd49  	 : out = 12'd2065  	 ;
   	 12'd50  	 : out = 12'd2107  	 ;
   	 12'd51  	 : out = 12'd2149  	 ;
   	 12'd52  	 : out = 12'd2191  	 ;
   	 12'd53  	 : out = 12'd2233  	 ;
   	 12'd54  	 : out = 12'd2275  	 ;
   	 12'd55  	 : out = 12'd2317  	 ;
   	 12'd56  	 : out = 12'd2360  	 ;
   	 12'd57  	 : out = 12'd2402  	 ;
   	 12'd58  	 : out = 12'd2444  	 ;
   	 12'd59  	 : out = 12'd2486  	 ;
   	 12'd60  	 : out = 12'd2528  	 ;
   	 12'd61  	 : out = 12'd2570  	 ;
   	 12'd62  	 : out = 12'd2612  	 ;
   	 12'd63  	 : out = 12'd2655  	 ;
   	 12'd64  	 : out = 12'd2697  	 ;
   	 12'd65  	 : out = 12'd2739  	 ;
   	 12'd66  	 : out = 12'd2781  	 ;
   	 12'd67  	 : out = 12'd2823  	 ;
   	 12'd68  	 : out = 12'd2865  	 ;
   	 12'd69  	 : out = 12'd2907  	 ;
   	 12'd70  	 : out = 12'd2950  	 ;
   	 12'd71  	 : out = 12'd2992  	 ;
   	 12'd72  	 : out = 12'd3034  	 ;
   	 12'd73  	 : out = 12'd3076  	 ;
   	 12'd74  	 : out = 12'd3118  	 ;
   	 12'd75  	 : out = 12'd3160  	 ;
   	 12'd76  	 : out = 12'd3202  	 ;
   	 12'd77  	 : out = 12'd3245  	 ;
   	 12'd78  	 : out = 12'd3287  	 ;
   	 12'd79  	 : out = 12'd3329  	 ;
   	 12'd80  	 : out = 12'd3371  	 ;
   	 12'd81  	 : out = 12'd3413  	 ;
   	 12'd82  	 : out = 12'd3455  	 ;
   	 12'd83  	 : out = 12'd3497  	 ;
   	 12'd84  	 : out = 12'd3540  	 ;
   	 12'd85  	 : out = 12'd3582  	 ;
   	 12'd86  	 : out = 12'd3624  	 ;
   	 12'd87  	 : out = 12'd3666  	 ;
   	 12'd88  	 : out = 12'd3708  	 ;
   	 12'd89  	 : out = 12'd3750  	 ;
   	 12'd90  	 : out = 12'd3792  	 ;
   	 12'd91  	 : out = 12'd3835  	 ;
   	 12'd92  	 : out = 12'd3877  	 ;
   	 12'd93  	 : out = 12'd3919  	 ;
   	 12'd94  	 : out = 12'd3961  	 ;
   	 12'd95  	 : out = 12'd4003  	 ;
   	 12'd96  	 : out = 12'd4045  	 ;
   	 12'd97  	 : out = 12'd4088  	 ;

				default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule

module B_5( input        [11:0] in ,
                   output logic [15:0] dval);

logic[11:0] out;

sinelookup sine1 (.in(out), .out(dval));
 // This module will be synthesized into a RAM
	always_comb begin
    unique case (in)
12'd0  	 : out = 12'd0  	 ;
   	 12'd1  	 : out = 12'd84  	 ;
   	 12'd2  	 : out = 12'd168  	 ;
   	 12'd3  	 : out = 12'd252  	 ;
   	 12'd4  	 : out = 12'd337  	 ;
   	 12'd5  	 : out = 12'd421  	 ;
   	 12'd6  	 : out = 12'd505  	 ;
   	 12'd7  	 : out = 12'd590  	 ;
   	 12'd8  	 : out = 12'd674  	 ;
   	 12'd9  	 : out = 12'd758  	 ;
   	 12'd10  	 : out = 12'd842  	 ;
   	 12'd11  	 : out = 12'd927  	 ;
   	 12'd12  	 : out = 12'd1011  	 ;
   	 12'd13  	 : out = 12'd1095  	 ;
   	 12'd14  	 : out = 12'd1180  	 ;
   	 12'd15  	 : out = 12'd1264  	 ;
   	 12'd16  	 : out = 12'd1348  	 ;
   	 12'd17  	 : out = 12'd1432  	 ;
   	 12'd18  	 : out = 12'd1517  	 ;
   	 12'd19  	 : out = 12'd1601  	 ;
   	 12'd20  	 : out = 12'd1685  	 ;
   	 12'd21  	 : out = 12'd1770  	 ;
   	 12'd22  	 : out = 12'd1854  	 ;
   	 12'd23  	 : out = 12'd1938  	 ;
   	 12'd24  	 : out = 12'd2022  	 ;
   	 12'd25  	 : out = 12'd2107  	 ;
   	 12'd26  	 : out = 12'd2191  	 ;
   	 12'd27  	 : out = 12'd2275  	 ;
   	 12'd28  	 : out = 12'd2360  	 ;
   	 12'd29  	 : out = 12'd2444  	 ;
   	 12'd30  	 : out = 12'd2528  	 ;
   	 12'd31  	 : out = 12'd2612  	 ;
   	 12'd32  	 : out = 12'd2697  	 ;
   	 12'd33  	 : out = 12'd2781  	 ;
   	 12'd34  	 : out = 12'd2865  	 ;
   	 12'd35  	 : out = 12'd2950  	 ;
   	 12'd36  	 : out = 12'd3034  	 ;
   	 12'd37  	 : out = 12'd3118  	 ;
   	 12'd38  	 : out = 12'd3203  	 ;
   	 12'd39  	 : out = 12'd3287  	 ;
   	 12'd40  	 : out = 12'd3371  	 ;
   	 12'd41  	 : out = 12'd3455  	 ;
   	 12'd42  	 : out = 12'd3540  	 ;
   	 12'd43  	 : out = 12'd3624  	 ;
   	 12'd44  	 : out = 12'd3708  	 ;
   	 12'd45  	 : out = 12'd3793  	 ;
   	 12'd46  	 : out = 12'd3877  	 ;
   	 12'd47  	 : out = 12'd3961  	 ;
   	 12'd48  	 : out = 12'd4045  	 ;

				default: begin
					out = 12'bZZZZZZZZZZZ;
				end
	
    endcase
	 end
endmodule
