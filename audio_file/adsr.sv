module adsr_controller( input logic[2:0] adsr_state,
								input logic[31:0] adsr_fp,
								output logic[31:0] adsr_fp_out,
								input logic[15:0] sample, 
								input logic key_release,
								output logic[15:0] sample_out,
								
								output logic att_done, dec_done, sus_done, rel_done);
								
logic [31:0] rate;
logic [31:0] fp_m;
logic [31:0] added;		
qadd myadd(.a(adsr_fp),
			  .b(rate),
			  .c(added));
fp_multiplier (.sample(sample),
					.fixed_point(fp_m),
					.out(sample_out));
					
			always_comb begin
				
				unique case(adsr_state)
					3'd0: begin
							rate = 32'h000000D1;

							if(adsr_fp > 32'h10000) begin
								fp_m = 32'h10000;
								adsr_fp_out = fp_m;
								att_done  = 1'b1;
								dec_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
							end else	begin
								fp_m = adsr_fp;
								att_done = 1'b0;
								dec_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
								
								adsr_fp_out = added;
							end					
					end
					3'd1: begin
						
							if(adsr_fp <= 32'h0000E000)begin
								fp_m = 32'h0000E0000;
								rate = 32'd0;
								adsr_fp_out = fp_m;
								dec_done = 1'b1;
								att_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
							end
							else begin
								rate = 32'h00000CCC;
					
								fp_m = adsr_fp;
								adsr_fp_out = adsr_fp - rate;
								att_done = 1'b0;
								dec_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
							end
					end
					3'd2: begin
							rate = 32'd0;
							fp_m = adsr_fp;
							adsr_fp_out = 32'h0000E000;
					
							if(key_release) begin
								sus_done = 1'b1;
								att_done = 1'b0;
								dec_done = 1'b0;
				
								rel_done = 1'b0;
							end
							else begin	
								att_done = 1'b0;
								dec_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
							end
					end
				   3'd3: begin
							
							if(adsr_fp <= 32'd0 || (adsr_fp[31] == 1'b1)) begin
								fp_m = 32'd0;
								rate = 32'd0;
								
								adsr_fp_out = 32'd0;
								rel_done = 1'b1;
								att_done = 1'b0;
								dec_done = 1'b0;
								sus_done = 1'b0;
		
							end
							else begin
								rate = 32'h00000008;
								fp_m = adsr_fp;
								adsr_fp_out = adsr_fp - rate;
								att_done = 1'b0;
								dec_done = 1'b0;
								sus_done = 1'b0;
								rel_done = 1'b0;
							end 			
					end
					default: begin
							fp_m = 32'h00010000;
							adsr_fp_out = adsr_fp;
							rate = 32'd0;
							att_done = 1'b0;
							dec_done = 1'b0;
							sus_done = 1'b0;
						   rel_done = 1'b1;
					end
				endcase
				end 
endmodule
