module key_wave_adder(	input logic[15:0] a ,
								input logic[15:0] b,
								output logic[15:0] out);

logic[31:0] add;
logic [31:0] temp;
assign out = temp[15:0];
assign add = {{16{b[15]}}, b} + {{16{a[15]}}, a};

always_comb 
		begin	
				if(a == 32'b0)
					temp = add;
					
				else if(b == 32'b0)
					temp = add;
					
				else
					temp = add/32'b10;
		end

assign out = temp[15:0];
endmodule
