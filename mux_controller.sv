module muxcounter( input logic[15:0] sample_c4, 
						sample_d4, sample_e4, sample_f4, 
						sample_g4, sample_a4, sample_b4, 
						sample_c5, sample_d5, sample_e5, 
						sample_f5, sample_g5, sample_a5, 
						sample_b5, sample_c6, 
						input logic [3:0] counter, 
						input logic active_a, active_s, active_d, active_f, active_j, active_k, active_l, active_colon,
						output logic key_c4, key_c5, key_c6, key_d4, key_d5, key_e4, key_e5, key_f4, key_f5, key_g4, key_g5, 
						key_a4, key_a5, key_b4, key_b5,
						output logic [15:0] out1, out2,out3,out4,out5,out6,out7,out8);
always_comb
begin 
	unique case(counter[3:0])
	4'd0:
		begin 
		out1 = sample_c4;
		out2 = sample_d4;
		out3 = sample_e4;
		out4 = sample_f4;
		out5 = sample_g4;
		out6 = sample_a4;
		out7 = sample_b4;
		out8 = sample_c5;
		
		key_c4 = active_a; 
		key_d4 = active_s;
		key_e4 = active_d; 
		key_f4 = active_f; 
		key_g4 = active_j; 
		key_a4 = active_k; 
		key_b4 = active_l; 
		key_c5 = active_colon;
		key_d5 = 1'bZ;
		key_e5 = 1'bZ; 
		key_f5 = 1'bZ; 
		key_g5 = 1'bZ; 
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
		
	4'd1:
		begin 
		out1 = sample_d4;
		out2 = sample_e4;
		out3 = sample_f4;
		out4 = sample_g4;
		out5 = sample_a4;
		out6 = sample_b4;
		out7 = sample_c5;
		out8 = sample_d5;
		
		key_c4 = 1'bZ;
		key_d4 = active_a; 
		key_e4 = active_s;
		key_f4 = active_d; 
		key_g4 = active_f; 
		key_a4 = active_j; 
		key_b4 = active_k; 
		key_c5 = active_l; 
		key_d5 = active_colon;
		key_e5 = 1'bZ; 
		key_f5 = 1'bZ; 
		key_g5 = 1'bZ; 
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
		
		4'd2:
		begin 
		out1 = sample_e4;
		out2 = sample_f4;
		out3 = sample_g4;
		out4 = sample_a4;
		out5 = sample_b4;
		out6 = sample_c5;
		out7 = sample_d5;
		out8 = sample_e5;
		
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = active_a; 
		key_f4 = active_s;
		key_g4 = active_d; 
		key_a4 = active_f; 
		key_b4 = active_j; 
		key_c5 = active_k; 
		key_d5 = active_l; 
		key_e5 = active_colon;
		key_f5 = 1'bZ; 
		key_g5 = 1'bZ; 
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
		
		4'd3:
		begin 
		out1 = sample_f4;
		out2 = sample_g4;
		out3 = sample_a4;
		out4 = sample_b4;
		out5 = sample_c5;
		out6 = sample_d5;
		out7 = sample_e5;
		out8 = sample_f5;
		
		
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = 1'bZ; 
		key_f4 = active_a; 
		key_g4 = active_s;
		key_a4 = active_d; 
		key_b4 = active_f; 
		key_c5 = active_j; 
		key_d5 = active_k; 
		key_e5 = active_l; 
		key_f5 = active_colon;
		key_g5 = 1'bZ; 
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
		
		4'd4:
		begin 
		out1 = sample_g4;
		out2 = sample_a4;
		out3 = sample_b4;
		out4 = sample_c5;
		out5 = sample_d5;
		out6 = sample_e5;
		out7 = sample_f5;
		out8 = sample_g5;
		
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = 1'bZ; 
		key_f4 = 1'bZ;  
		key_g4 = active_a; 
		key_a4 = active_s;
		key_b4 = active_d; 
		key_c5 = active_f; 
		key_d5 = active_j; 
		key_e5 = active_k; 
		key_f5 = active_l; 
		key_g5 = active_colon;
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;	
		end
		
		4'd5:
		begin 
		out1 = sample_a4;
		out2 = sample_b4;
		out3 = sample_c5;
		out4 = sample_d5;
		out5 = sample_e5;
		out6 = sample_f5;
		out7 = sample_g5;
		out8 = sample_a5;
		
		
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = 1'bZ; 
		key_f4 = 1'bZ; 
		key_g4 = 1'bZ;  
		key_a4 = active_a; 
		key_b4 = active_s;
		key_c5 = active_d; 
		key_d5 = active_f; 
		key_e5 = active_j; 
		key_f5 = active_k; 
		key_g5 = active_l; 
		key_a5 = active_colon;
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
		
		4'd6:
		begin 
		out1 = sample_b4;
		out2 = sample_c5;
		out3 = sample_d5;
		out4 = sample_e5;
		out5 = sample_f5;
		out6 = sample_g5;
		out7 = sample_a5;
		out8 = sample_b5;
		
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = 1'bZ; 
		key_f4 = 1'bZ; 
		key_g4 = 1'bZ; 
		key_a4 = 1'bZ; 
		key_b4 = active_a; 
		key_c5 = active_s;
		key_d5 = active_d; 
		key_e5 = active_f; 
		key_f5 = active_j; 
		key_g5 = active_k; 
		key_a5 = active_l; 
		key_b5 = active_colon;
	   key_c6 = 1'bZ;
		end
		
		4'd7:
		begin
		out1 = sample_c5;
		out2 = sample_d5;
		out3 = sample_e5;
		out4 = sample_f5;
		out5 = sample_g5;
		out6 = sample_a5;
		out7 = sample_b5;
		out8 = sample_c6;
		key_c4 = 1'bZ;
		key_d4 = 1'bZ;
		key_e4 = 1'bZ; 
		key_f4 = 1'bZ; 
		key_g4 = 1'bZ; 
		key_a4 = 1'bZ; 
		key_b4 = 1'bZ;	
		key_c5 = active_a; 
		key_d5 = active_s;
		key_e5 = active_d; 
		key_f5 = active_f; 
		key_g5 = active_j; 
		key_a5 = active_k; 
		key_b5 = active_l; 
		key_c6 = active_colon; 
		end
		
		default:
		begin 
		out1 = sample_c4;
		out2 = sample_d4;
		out3 = sample_e4;
		out4 = sample_f4;
		out5 = sample_g4;
		out6 = sample_a4;
		out7 = sample_b4;
		out8 = sample_c5;
		
		key_c4 = active_a; 
		key_d4 = active_s;
		key_e4 = active_d; 
		key_f4 = active_f; 
		key_g4 = active_j; 
		key_a4 = active_k; 
		key_b4 = active_l; 
		key_c5 = active_colon;
		key_d5 = 1'bZ;
		key_e5 = 1'bZ; 
		key_f5 = 1'bZ; 
		key_g5 = 1'bZ; 
		key_a5 = 1'bZ; 
		key_b5 = 1'bZ; 
		key_c6 = 1'bZ;
		end
	endcase		
end

endmodule
