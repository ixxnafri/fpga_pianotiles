////-------------------------------------------------------------------------
////    Color_Mapper.sv                                                    --
////    Stephen Kempf                                                      --
////    3-1-06                                                             --
////                                                                       --
////    Modified by David Kesler  07-16-2008                               --
////    Translated by Joe Meng    07-07-2013                               --
////    Modified by Po-Han Huang  10-06-2017                               --
////                                                                       --
////    Fall 2017 Distribution                                             --
////                                                                       --
////    For use with ECE 385 Lab 8                                         --
////    University of Illinois ECE Department                              --
////-------------------------------------------------------------------------
//
//// color_mapper: Decide which color to be output to VGA for each pixel.
//module  color_mapper ( input              is_ball_1, is_ball_2, is_ball_3, is_ball_4, is_ball_5, is_ball_6, is_ball_7, is_ball_8,            // Whether current pixel belongs to ball 
//                       input        [9:0] DrawX, DrawY,       // Current pixel coordinates
//							  input [15:0] keycode, 
//							  input [15:0] keycode_2,
//							  input [15:0] keycode_3,
//                       output logic [7:0] VGA_R, VGA_G, VGA_B // VGA RGB output
//                     );
//    
//    logic [7:0] Red, Green, Blue;
//    
//    // Output colors to VGA
//    assign VGA_R = Red;
//    assign VGA_G = Green;
//    assign VGA_B = Blue;
//    
//    // Assign color based on is_ball signal
//    always_comb
//    begin
//        begin
//            Red = 8'd212;
//            Green = 8'd175;
//            Blue = 8'd55;
//        end
//		  
//		  else if (	DrawX == 10'd0 	|| DrawX == 10'd79	|| DrawX == 10'd80 	|| DrawX == 10'd159 || DrawX == 10'd160|| DrawX == 10'd239 || 
//						DrawX == 10'd240 	|| DrawX == 10'd319 	|| DrawX == 10'd320 	|| DrawX == 10'd399 || DrawX == 10'd400|| DrawX == 10'd479 || 
//						DrawX == 10'd480 	|| DrawX == 10'd559 || DrawX == 10'd560   || DrawX == 10'd639 || DrawY == 10'd359|| DrawY == 10'd358
//						)
//		  begin
//		      Red 	= 8'h20;
//            Green = 8'h20;
//            Blue 	= 8'h20;
//		  end
//		  
//		  else if(((DrawY > 10'd359) && (DrawX < 10'd79)) || 
//					((DrawY > 10'd359) && (DrawX > 10'd160 && DrawX < 10'd239)) ||
//					((DrawY > 10'd359) && (DrawX > 10'd320 && DrawX < 10'd399)) ||
//					((DrawY > 10'd359) && (DrawX > 10'd480 && DrawX < 10'd559))
//					)
//		  begin
//		  		Red 	= 8'd19;
//            Green = 8'd41;
//            Blue 	= 8'd75;
//		  end
//		  
//		  else if((((DrawY > 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159))) || 
//					 (((DrawY > 10'd359) && (DrawX > 10'd240 && DrawX < 10'd319))) || 
//					 (((DrawY > 10'd359) && (DrawX > 10'd400 && DrawX < 10'd479))) ||
//					 (((DrawY > 10'd359) && (DrawX > 10'd560 && DrawX < 10'd639)))
//					)
//		  begin
//		  		Red 	= 8'd232;
//            Green = 8'd74;
//            Blue 	= 8'd39;
//		  end
//		  
//		  else if(	keycode[7:0]  		== 8'h04 || keycode[7:0]  		== 8'h16 || keycode[7:0]  		== 8'h07 || keycode[7:0]  		== 8'h09 || keycode[7:0]  		== 8'h0d || keycode[7:0]  		== 8'h0e || keycode[7:0]  		== 8'h0f || keycode[7:0]  		== 8'h33 ||     
//						keycode[15:8] 		== 8'h04 || keycode[15:8] 		== 8'h16 || keycode[15:8] 		== 8'h07 || keycode[15:8] 		== 8'h09 || keycode[15:8] 		== 8'h0d || keycode[15:8] 		== 8'h0e || keycode[15:8] 		== 8'h0f || keycode[15:8] 		== 8'h33 ||
//						keycode_2[7:0] 	== 8'h04 || keycode_2[7:0] 	== 8'h16 || keycode_2[7:0] 	== 8'h07 || keycode_2[7:0] 	== 8'h09 || keycode_2[7:0] 	== 8'h0d || keycode_2[7:0] 	== 8'h0e || keycode_2[7:0] 	== 8'h0f || keycode_2[7:0] 	== 8'h33 ||
//						keycode_2[15:8] 	== 8'h04 || keycode_2[15:8] 	== 8'h16 || keycode_2[15:8] 	== 8'h07 || keycode_2[15:8] 	== 8'h09 || keycode_2[15:8] 	== 8'h0d || keycode_2[15:8] 	== 8'h0e || keycode_2[15:8] 	== 8'h0f || keycode_2[15:8] 	== 8'h33 || 
//						keycode_3[7:0] 	== 8'h04 || keycode_3[7:0] 	== 8'h16 || keycode_3[7:0] 	== 8'h07 || keycode_3[7:0] 	== 8'h09 || keycode_3[7:0] 	== 8'h0d || keycode_3[7:0] 	== 8'h0e || keycode_3[7:0] 	== 8'h0f || keycode_3[7:0] 	== 8'h33 || 
//						keycode_3[15:8] 	== 8'h04 || keycode_3[15:8] 	== 8'h16 || keycode_3[15:8] 	== 8'h07 || keycode_3[15:8] 	== 8'h09 || keycode_3[15:8] 	== 8'h0d || keycode_3[15:8] 	== 8'h0e || keycode_3[15:8] 	== 8'h0f || keycode_3[15:8] 	== 8'h33   )
//		  begin
//				if(keycode[7:0] == 8'h04 || keycode[15:8] == 8'h04 || keycode_2[7:0] == 8'h04 || keycode_2[15:8] == 8'h04 || keycode_3[7:0] == 8'h04 || keycode_3[15:8] == 8'h04)
//				begin			
//					if(((DrawY < 10'd359) && (DrawX < 10'd79)))
//						begin
//						Red = 8'd173;
//						Green = 8'd255;
//						Blue = 8'd255;
//						end
//				end
//		  ////////////////////////////////////////////////////////////////////////////////
//				if(keycode[7:0] == 8'h16 || keycode[15:8] == 8'h16 || keycode_2[7:0] == 8'h16 || keycode_2[15:8] == 8'h16 || keycode_3[7:0] == 8'h16 || keycode_3[15:8] == 8'h16)
//				begin			
//					if((((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159))))
//						begin
//						Red = 8'd173;
//						Green = 8'd255;
//						Blue = 8'd255;
//						end
//				end
//		  ///////////////////////////////////////////////////////////////////////////////
//				else;
//				
//		  end
//
//		 
//		  else
//        begin
//            Red   = 8'h48; 
//            Green = 8'h48;
//            Blue  = 8'h48;
//		  end
//		  
//    end 
//    
//endmodule\
