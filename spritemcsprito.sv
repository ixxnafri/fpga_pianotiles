
module  sprite( input         Clk,                // 50 MHz clock
                             Reset,              // Active-high reset signal
                             frame_clk,          // The clock indicating a new frame (~60Hz)
               input [9:0]   DrawX, DrawY,       // Current pixel coordinates
					output logic [18:0]  read_address
              );
   
always_latch

begin
	if(DrawY > 100 & DrawY < 377)
	 read_address = DrawX + (DrawY-10'd100)*(10'd640);

 end   
endmodule

    