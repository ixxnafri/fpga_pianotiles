module numcounter(	input  KEY1,KEY2,KEY3, Clk, 
							output logic [3:0] counter
						);
						
logic ack2,ack3;
always_ff @ (posedge Clk)
	begin
	if(KEY1 == 1'b1)
	begin
		counter = 4'b0;
	end
	else if(KEY2 == 1'b1)
	begin
		ack2 = 1'b1;
	end
	else if(KEY3 == 1'b1)
	begin
		ack3 = 1'b1;
	end
	
	else
		counter = counter;
	
	
	if(ack2 == 1'b1 && KEY2 == 1'b0)
	begin
		counter = counter + 1'b1;
		ack2 = 1'b0;
	end
	
	if(ack3 == 1'b1 && KEY3 == 1'b0)
	begin
		counter = counter - 1'b1;
		ack3 = 1'b0;
	end
end
	
	
endmodule
