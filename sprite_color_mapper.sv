
// color_mapper: Decide which color to be output to VGA for each pixel.
module  color_mapper_sprite ( 
                       input        [9:0] DrawX, DrawY,       // Current pixel coordinates
							  input 			[4:0] data_Out,
                       output logic [7:0] VGA_R1, VGA_G1, VGA_B1 // VGA RGB output
                     );
    
    logic [7:0] Red, Green, Blue;
    
    // Output colors to VGA
    assign VGA_R1 = Red;
    assign VGA_G1 = Green;
    assign VGA_B1 = Blue;
    
    // Assign color based on is_ball signal
    always_comb
	 begin
	 unique case(data_Out)
		5'd1:
		begin
			Red 		= 8'd0
			; 
			Green		= 8'd0;
			Blue		= 8'd0;
		end
		
		default:
		begin
			Red 		= 8'd255 - DrawX[7:0]; 
			Green		= 8'd255 - 8'd30;
			Blue		= 8'd255 - DrawY[7:0];
		end
		endcase
		
	if(DrawY < 10'd100 || DrawY > 10'd367)
		begin
			Red 		= 8'd0; 
			Green		= 8'd0;
			Blue		= 8'd0;
		end
	end 
endmodule
