//-------------------------------------------------------------------------
//    Color_Mapper.sv                                                    --
//    Stephen Kempf                                                      --
//    3-1-06                                                             --
//                                                                       --
//    Modified by David Kesler  07-16-2008                               --
//    Translated by Joe Meng    07-07-2013                               --
//    Modified by Po-Han Huang  10-06-2017                               --
//                                                                       --
//    Fall 2017 Distribution                                             --
//                                                                       --
//    For use with ECE 385 Lab 8                                         --
//    University of Illinois ECE Department                              --
//-------------------------------------------------------------------------

// color_mapper: Decide which color to be output to VGA for each pixel.
module  color_mapper ( input              is_ball_1, is_ball_2, is_ball_3, is_ball_4, is_ball_5, is_ball_6, is_ball_7, is_ball_8, 
														is_ball_9, is_ball_10, is_ball_11, is_ball_12, is_ball_13, is_ball_14, is_ball_15, is_ball_16,           // Whether current pixel belongs to ball 
                       input        [9:0] DrawX, DrawY,       // Current pixel coordinates
							  input [15:0] keycode, keycode_2, keycode_3, 
                       output logic [7:0] VGA_R2, VGA_G2, VGA_B2 // VGA RGB output
                     );
    
    logic [7:0] Red, Green, Blue;
    
    // Output colors to VGA
    assign VGA_R2 = Red;
    assign VGA_G2 = Green;
    assign VGA_B2 = Blue;
    
    // Assign color based on is_ball signal
    always_comb
    begin
        if (is_ball_1 == 1'b1 || is_ball_2 == 1'b1 || is_ball_3 == 1'b1 || is_ball_4 == 1'b1 || is_ball_5 == 1'b1 || is_ball_6 == 1'b1 || is_ball_7 == 1'b1 || is_ball_8 == 1'b1 ||
				is_ball_9 == 1'b1 || is_ball_10 == 1'b1 || is_ball_11 == 1'b1 || is_ball_12 == 1'b1 || is_ball_13 == 1'b1 || is_ball_14 == 1'b1 || is_ball_15 == 1'b1 || is_ball_16 == 1'b1) 
        begin
            Red = 8'd212;
            Green = 8'd175;
            Blue = 8'd55;
        end
		  
		  else if (	DrawX == 10'd0 	|| DrawX == 10'd79	|| DrawX == 10'd80 	|| DrawX == 10'd159 || DrawX == 10'd160|| DrawX == 10'd239 || 
						DrawX == 10'd240 	|| DrawX == 10'd319 	|| DrawX == 10'd320 	|| DrawX == 10'd399 || DrawX == 10'd400|| DrawX == 10'd479 || 
						DrawX == 10'd480 	|| DrawX == 10'd559 || DrawX == 10'd560   || DrawX == 10'd639 || DrawY == 10'd359|| DrawY == 10'd358
						)
		  begin
		      Red 	= 8'h20;
            Green = 8'h20;
            Blue 	= 8'h20;
		  end
		  
		  else if(((DrawY > 10'd359) && (DrawX < 10'd79)) || 
					((DrawY > 10'd359) && (DrawX > 10'd160 && DrawX < 10'd239)) ||
					((DrawY > 10'd359) && (DrawX > 10'd320 && DrawX < 10'd399)) ||
					((DrawY > 10'd359) && (DrawX > 10'd480 && DrawX < 10'd559))
					)
		  begin
		  		Red 	= 8'd19;
            Green = 8'd41;
            Blue 	= 8'd75;
		  end
		  
		  else if((((DrawY > 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159))) || 
					 (((DrawY > 10'd359) && (DrawX > 10'd240 && DrawX < 10'd319))) || 
					 (((DrawY > 10'd359) && (DrawX > 10'd400 && DrawX < 10'd479))) ||
					 (((DrawY > 10'd359) && (DrawX > 10'd560 && DrawX < 10'd639)))
					)
		  begin
		  		Red 	= 8'd232;
            Green = 8'd74;
            Blue 	= 8'd39;
		  end
		  
        else 
        begin
		  unique case(keycode[15:0])
		  
		  
		  8'h04: //A 
		  begin
		  if((DrawY < 10'd359) && (DrawX < 10'd79))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  8'h16: //S
		  begin
		  if((((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159))))
		  begin
		      Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  8'h07: //D
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160 && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  8'h09: //F
		  begin
		  if((((DrawY < 10'd359) && (DrawX > 10'd240 && DrawX < 10'd319))))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  8'h0d: // J
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320 && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  8'h0E: //K
		  begin
		  if((((DrawY < 10'd359) && (DrawX > 10'd400 && DrawX < 10'd479))))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  8'h0F: // L
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480 && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end

		  8'h33:
		  begin
		  if((((DrawY < 10'd359) && (DrawX > 10'd560 && DrawX < 10'd639))))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end		  
		  end
		  
		  16'h1604: //A - S
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0704: //A - D
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0904: //A - F
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0d04: //A - J
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0E04: //A - K
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0F04: //A - L
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h3304: //A - ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX < 10'd79) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0716: //S - D;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0416: //S - A;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0916: //S - F;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0d16: //S - j;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0E16: //S - K;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0F16: //S - L;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h3316: //S - ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd80  && DrawX < 10'd159) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0407: //d - a;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h1607: //d - s;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd90  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0907: //d - f;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0d07: //d - J;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h0E07: //d - k;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0F07: //d - l;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h3307: //d - ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd160  && DrawX < 10'd239) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		   16'h0409: //F-a ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		   16'h1609: //F-s ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		   16'h0709: //F-d ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0D09: //F-J ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h0E09: //F-k ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0F09: //F-L ;
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h3309: //F-; 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd240  && DrawX < 10'd319) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		   16'h040d: //J- a 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		   16'h160d: //J- s 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		   16'h070d: //J- d 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		   16'h090d: //J- F 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  
		  16'h0E0d: //J- K 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0F0d: //J- L 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h330d: //J- ; 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd320  && DrawX < 10'd399) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h040E: //K- A 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h160E: //K- S 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h070E: //K- D 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h090E: //K- F 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h0d0E: //K- J 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0F0E: //K- L 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0E0F: //L- K 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red  = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h330E: //K - ; 
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd400  && DrawX < 10'd479) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h040F: //L- A  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h160F: //L- S  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h070F: //L- D  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h090F: //L- F  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0d0F: //L- K  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  16'h330F: //L- ;  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd480  && DrawX < 10'd559) || (DrawX > 10'd560  && DrawX < 10'd639)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  
		  
		  16'h0433: //; - A   
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd0  && DrawX < 10'd79)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h1633: //; - S   
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd80  && DrawX < 10'd159)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0733: //; - D   
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd160  && DrawX < 10'd239)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0933: //; - F   
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd240  && DrawX < 10'd319)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  
		  16'h0d33: //; - J  
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd320  && DrawX < 10'd399)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0E33: //; - K   
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd400  && DrawX < 10'd479)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  16'h0F33: //; - L
		  begin
		  if(((DrawY < 10'd359) && (DrawX > 10'd560  && DrawX < 10'd639) || (DrawX > 10'd480  && DrawX < 10'd559)))
		  begin
            Red = 8'd173;
            Green = 8'd255;
            Blue = 8'd255;
		  end
		  else
			begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
			end
		  end
		  
		  default:
		  begin
				// Background with nice color gradient
            Red   = 8'h48; 
            Green = 8'h48;
            Blue  = 8'h48;
				//Blue = 8'h7f - {1'b0, DrawX[9:3]};
		  end
		  endcase
        end
    end 
    
endmodule
